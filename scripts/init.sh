#!/bin/bash

cat <<%EOF% >$HOME/.ssh/config
StrictHostKeyChecking no
UserKnownHostsFile /dev/null
LogLevel error
User ubuntu

Host generic
    Hostname 10.6.37.15
Host docker
    Hostname 10.6.37.16   
Host ms
    Hostname 10.6.37.20
Host k8s
    Hostname 10.6.37.30 
Host w01
    Hostname 10.6.37.31
%EOF%


# Alias
alias tf='terraform'
alias tfa='terraform apply -auto-approve'
alias tfd='terraform destroy -auto-approve'
alias tfi='terraform init'
alias tfo='terraform output'
alias tfp='terraform plan'

cat <<%EOF% >>$HOME/.bashrc
alias tf='terraform'
alias tfa='terraform apply -auto-approve'
alias tfd='terraform destroy -auto-approve'
alias tfi='terraform init'
alias tfo='terraform output'
alias tfp='terraform plan'

export CDPATH=${GITPOD_REPO_ROOT}/2_Unterrichtsressourcen:$CDPATH
%EOF%

export CDPATH=${GITPOD_REPO_ROOT}/2_Unterrichtsressourcen:$CDPATH
