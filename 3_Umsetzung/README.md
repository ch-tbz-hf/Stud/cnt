# Modulablauf 

[[_TOC_]]

## 1. MAAS – Metal as a Service und Cloud

* Virtualisierung und Virtuelle Maschinen
* Automatisierung mittels «Infrastructure as Code» und «Cloud-init»
* Cloud-Servicemodelle
* Dynamische Infrastrukturen mittels Metal as a Service zur Verfügung stellen

**Transfer**: Anwendung durch Aufsetzen und Steuerung einer privaten Hardware-Cloud im eigenen Rechenzentrum. 

### Lektionenplan

| Anzahl Lektionen | Themen                                                                 | Kompetenzen | Tools                                |
|------------------|------------------------------------------------------------------------|-------------|--------------------------------------|
|       5 + 4      | [Virtualisierung und Virtuelle Maschinen](../2_Unterrichtsressourcen/A/)<br>[Infrastructure as Code](../2_Unterrichtsressourcen/B/) | A, B | WireGuard, MaaS.io, Cloud-init |
|       5          | [Cloud - Funktionsweise, Servicemodelle](../2_Unterrichtsressourcen/C/)<br>[Lift und Shift etc.](../2_Unterrichtsressourcen/C/)   | C | Azure, AWS Cloud, CLI, Cloud-init    |
|       5          | Hands-on [B](../2_Unterrichtsressourcen/B#hands-on) und [C](../2_Unterrichtsressourcen/C#hands-on)  | A,B,C  | Maas.io, Azure, AWS Cloud, Cloud-init|
|       9          | Praktische Arbeit: Infrastructure as Code<br>**Präsentation**          | A,B,C       | Azure, AWS Cloud, CLI, Cloud-init    |
|       4          | [Private Cloud Einrichten](../2_Unterrichtsressourcen/D)               | D           | Maas.io                              |
|       5          | Praktische Arbeit: Private Cloud Einrichten                            | D           | Maas.io                              |
|       5          | Private Cloud Erweitern<br>[Service Discovery](../2_Unterrichtsressourcen/E)<br>[Persistenz](../2_Unterrichtsressourcen/F/)  | E,F   | Maas.io, iptables, Reverse-Proxy, NFS|
|       5          | Private Cloud Erweitern<br>Selbständiges Arbeiten                      | D,E,F         | Maas.io, iptables, Reverse-Proxy, NFS|
|       9          | Praktische Arbeit: Private Cloud<br>**Präsentation**                   | D,E,F         | Maas.io, iptables, Reverse-Proxy, NFS|

**Total**: 51 (56) Lektionen

## 2. Container und Kubernetes, DevOps

* Containerisierung der Informatik
* Container Umgebungen
* Container Sicherheitskonzepte
* Container Cluster (Kubernetes) und Ressourcen
* DevOps (CI/CD, Rolling Updates)

**Transfer**: Aufbau und Betrieb eines Kubernetes-Clusters. 

### Lektionenplan

| Anzahl Lektionen | Themen                                                                                                    | Kompetenzen   | Tools                              |
|------------------|-----------------------------------------------------------------------------------------------------------|---------------|------------------------------------|
|        1         | [Einführung in Container, Linux Namespaces](../2_Unterrichtsressourcen/H)                                 | H             | MaaS.io, Cloud-init, Docker        |
|        1         | [Container Laufzeitumgebungen, Container Images erstellen und mit Registries arbeiten](../2_Unterrichtsressourcen/I)| I   | MaaS.io, Cloud-init, Docker        |
|        4         | Praktische Arbeit: Container Images und Registries<br>**Präsentation**                                    | H, I          | Container, Docker                  |
|        4         | [Architekturstyles für Container (Microservices)](../2_Unterrichtsressourcen/J)                           | J             | MaaS.io, Cloud-init, Kubernetes    |
|        5         | [Kubernetes (K8s) Einführung und Aufsetzung eines K8s Clusters](../2_Unterrichtsressourcen/K)             | K             | MaaS.io, Cloud-init, Kubernetes    |
|        2         | [Kubernetes Starten und Betreiben von Container (Pod, Services, Ingress, ReplicaSet)](../2_Unterrichtsressourcen/K)   | H, I, J, K    | Container, Kubernetes              |
|        1         | [Persistenz mit Kubernetes (PersistentVolume und Claim)](../2_Unterrichtsressourcen/K)                    | H, I, J, K    | Container, Kubernetes              |
|        2         | [Continuous Integration / Delivery mit Kubernetes (Deployment)](../2_Unterrichtsressourcen/L) | H, I, J, K, L | Container, Kubernetes              |
|        9         | Praktische Arbeit: Kubernetes<br>**Präsentation**                                                         | H, I, J, K, L | Container, Kubernetes              |

**Total**: 29 Lektionen

