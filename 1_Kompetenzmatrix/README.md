# Kompetenzmatrix - Modul CNT

[[_TOC_]]

## Handlungsziele und typische Handlungssituationen

### 1. MAAS – Metal as a Service und Cloud

##### 1.1. Analysiert die bestehende ICT-Umgebung und deren Struktur und beurteilt die Möglichkeit eine Private Cloud einzusetzen.

Die Server sind pro Abteilung organisiert und sollen, in eine Private Cloud, zentralisiert werden.

Dabei sollen auch Optimierungen, wie Eleminierung doppelter Applikationen, Services, Server, etc. durchgeführt werden.

Alle Server sollen als Virtuelle Maschinen (VMs) zur Verfügung stehen.

##### 1.2.  Kann erste Entscheidungshilfen für sein Unternehmen liefern welche Cloud Services sinnvoll sind 

Nicht alle Services sind sinnvoll, On-premise zu betreiben.

Welche der Services, in der Cloud, eigenen sich für welchen Einsatzzweck?

##### 1.3.  Der Studierende ist in der Lage, Systeme zu evaluieren, deren Performance und Eignung für den Cloud Anwendungszweck zu beurteilen.

Die eingesetzten Technologien und Betriebssysteme sind in die Jahre gekommen. Welche Technologien und Betriebssysteme eignen sich für den Cloud Einsatz?

##### 1.4.  Der Studierende ist in der Lage ein Cloud System basierend auf MAAS.io aufzusetzen und zu betreiben.

Das Management hat Entschieden eine Private Cloud aufzusetzen. Weil es Open Source ist, wurde als Produkt maas.io ausgewählt.

##### 1.5.  Der Studierende ist in der Lage Services für die Cloud bereitzustellen, diese zu automatisieren und jederzeit zur reproduzieren.

Das manuelle Aufsetzen der Server und Services ist zeitaufwendig und nicht mehr zeitgemäss. Für jeden neuen Mandanten wird sehr viel unnötige Zeit aufgewendet, welche nicht mehr verrechenbar ist. Darum sollen alle Services in VMs gezügelt und die Installation der Services automatisiert werden. Als Produkt kommt Cloud-init zum Einsatz.


#### Matrix

| Kompetenzband: | HZ  | Grundlagen                                  | Fortgeschritten                            | Erweitert                                   |
| ----------------|-----|------------------------------------------- | ------------------------------------------ | ------------------------------------------- |
| Grundlagen      | 1.1 | AG1: Ich kenne Services                    | AF1: Ich kann Services in VMs verteilen    | AE1: Ich kann VMs für Services einsetzen    |
|                 |     | AG2: Ich habe etwas von (SSH) Public/Private Keys gehört | AF2: Ich kann (SSH) Public/Private Key erklären | AE2: Ich kann selber (SSH) Public/Private Keys erstellen und einsetzen |
|                 |     | AG3: Ich kann mich mit einer VM verbinden  | AF3: Ich kann mich, mittels dem SSH Protokoll, mit einer VM verbinden | AE3: Ich kann einen SSH Key erstellen und diesen für die Verbindung zur VM verwenden |
|                 |     | AG4: Ich kann ein paar Linux Pakete aufzählen | AF4: Ich kann Linux Pakete installieren | AE4: Ich kann Linux Pakete suchen und installieren |
|                 |     | AG5: Ich kann ein paar Linux Befehle aufzählen | AF5: Ich kann ein paar Linux Befehle verwenden | AE5: Ich kann Linux Befehle einsetzen und deren Funktion erklären |  
| Automatisierung | 1.5 | BG1: Ich kenne Infrastructure as Code  | BF1: Ich kann Infrastructure as Code erklären | BE1: Ich kann Infrastructure as Code mit Beispielen erklären  |
|                 |     | BG2: Ich kenne Produkte für Automatisierung  | BF2: Ich kann VMs, anhand der Cloud-init Beispiele automatisiert aufsetzen | BE2: Ich kann VMs mit eigenen Cloud-init Scripten aufsetzen  |
|                 |     | BG3: Ich kenne Cloud-init        | BF3: Ich kenne die wichtigsten Cloud-init Einträge und habe diese Dokumentiert | BE3: Ich habe Cloud-init mit Beispielen Dokumentiert |
|                 |     | BG4: Ich habe von der Markdown Language YAML gehört | BF4: Ich kann den Aufbau einer YAML Datei erklären | BE4: Ich kann eine valide YAML Datei erstellen, z.B. mit VSCode |
|                 |     | BG5: Ich kenne Versionsverwaltungssysteme wie Git | BF5: Ich kann die Cloud-init Scripte in einem Git Repository ablegen | BE5: Ich kann die Cloud-init Scripte mit Dokumentation (z.B. als Markdown) in einem Git Repository ablegen |
|                 |     | BG6: Ich kenne Git Funktionen wie git clone | BF6: Ich kenne Git Funktionen zur Repository Pflege wie git pull, commit und push | BE6: Ich kann ein Git Repository mittels dem SSH Protokoll nachführen |       
| Cloud-Services  | 1.2 | CG1: Ich kenne die Cloud Services | CF1: Ich kann die Cloud Services erklären               | CE1: Ich kann die Cloud Services einsetzen   |
|                 |     | CG2: Ich kenne Cloud CLIs           | CF2: Ich kann die Funktionsweise von Cloud CLI erklären und habe diese Dokumentiert | CE2: Ich kann Cloud CLI Tools einsetzen und habe diese mit Beispielen Dokumentiert      |
|                 | 1.3 | CG3:                                       | CF3: Ich kann mittels Lift und Shift Services in die Cloud verschieben, mit Dokumentation | CE3: Ich kann mittels Lift und Reshape Services in die Cloud verschieben, mit Dokumentation |
|                 | 1.3 | CG4: Ich kann VMs auf einer Cloud erstellen | CF4: Ich kann VMs auf mindestens 2 Cloud erstellen | CE4: Ich kann VMs auf mindestens 3 Cloud erstellen |
| Private Cloud   | 1.4 | DG1: Ich kenne Metal as a Service | DF2: Ich kann Metal as a Service erklären, mit Dokumentation | DE1: Ich kann Metal as a Service mit Beispielen erklären | 
|                 |     | DG2: Ich kenne die Maas.io Umgebung  | DF2: Ich kann eine Maas.io Umgebung aufsetzen | DE2: Ich kann weitergehende Konzepte von Maas.io einsetzen, wie Region Controller, VLANs, Monitoring etc. |
|                 |     | DG3: Ich kenne VPN   | DF3: Ich kann VPN einsetzen, bzw. konfigurieren | DE3: Ich kann VPN mittels Metadata konfigurieren |
|                 |     | DG4: Ich kann eine abgesicherte Cloud Umgebung aufsetzen | DF4: Ich kann eine abgesicherte Hybrid Cloud Umgebung aufsetzen | DE4: Ich kann eine abgesicherte Multicloud Umgebung aufsetzen |
|                 |     | EG1: Ich kenne Service Discovery | EF1: Ich kann Service Discovery erklären | EE1: Ich kann die Service Discovery Umsetzungen einsetzen und dokumentieren |
|                 |     | FG1: Ich kenne Konzepte für die Persistenz von Daten | FF1: Ich kann Persistenz in Maas.io integrieren | FE1: Ich kann Persistenz und Sicherung der Daten implementieren und dokumentieren |  

### 2. Container und Kubernetes, DevOps

##### 2.1. Analysiert die bestehende ICT-Umgebung und deren Struktur und beurteilt die Möglichkeit Container einzusetzen.

In der Entwicklung sind verschiedene Architekturstyle anzutreffen. Von Monolitisch über "Service Orientiert Architektur" (SOA) bis zu Microservices.

Welche dieser Architekturstyles eignen sich für Container Umgebungen?

##### 2.2. Kennt Container Umgebungen

Container Umgebungen basieren Grundsätzlich auf der [Open Container Initiative (OCI)](https://opencontainers.org/).

Doch wie funktionieren Container Umgebungen, bzw. welche Technologien kommen zum Einsatz?

##### 2.3. Jeder Teilnehmer kann eine Container Umgebung aufsetzen.

Es gibt viele Möglichkeiten Container Umgebungen aufzusetzen.

Doch welche Umgebung / Produkte eignet sich für welchen Anwendungsfall?

Was sind Container Images und wie werden Applikationen (Microservices) darin verpackt?

Wie werden Container Images bezeichnet (Container Namesräume) und wo werden sie gespeichert?

##### 2.4. Jeder Teilnehmer kann eine Container Cluster (Kubernetes) Umgebung aufsetzen.

Kubernetes ist eine (Industrie) Plattform.

Jeder Cloud Service Anbieter, Linux Distributor etc. kann sich eine eigene Kubernetes Plattform zusammenstellen.

Gibt es ein Standardisierungs Gremium für Kubernetes?

Was muss ich beachten, damit meine Anwendung Distribution Neutral bleibt?

Verwende ich zum Aufsetzen eine Cloud Umgebung oder einen Installer?

Wo sind die Unterschiede zu einer Container Umgebung?

##### 2.5. Jeder Teilnehmer kann die Begriffe CI/CD (Continuous Integration / Delivery) einordnen 

Für den Betrieb von Kubernetes ist eine Continuous Integration / Delivery Pipeline wünschenswert, wenn nicht Voraussetzung.

Oft werden diese Begriff zusammen mit dem Buzzword "DevOps" verwendet.

Wie und wo sind diese Begriffe einzuordnen und was ist bereits in Kubernetes integriert?

#### Matrix

| Kompetenzband: | HZ  | Grundlagen                                  | Fortgeschritten                            | Erweitert                                   |
| ----------------|-----|------------------------------------------- | ------------------------------------------ | ------------------------------------------- |
| Container Technologien | 2.2 | HG1: Ich kenne die Open Container Initiative (OCI) | HF1: Ich kenne die Technologien hinter Container | HE1: Ich kann die Technologien hinter Container mit Beispielen erklären | 
| Container Umgebungen | 2.3 | IG1: Ich kenne Container Laufzeitumgebungen | IF1: Ich kann eine Container Laufzeitumgebungen aufsetzen | IE1: Ich kann Container Laufzeitumgebungen für verschiedene Anwendungsfälle aufsetzen | 
| Container Images | 2.3 | IG2: Ich kenne Container Images | IF2: Ich kann eine Container Images erklären | IE2: Ich kann Container Images erstellen | 
| Container Registry | 2.3 | IG3: Ich kenne Container Registries | IF3: Ich kann eine Container Registries erklären | IE3: Ich kann unterschiedliche Container Registries verwenden | 
| Architektur     | 2.1 | JG1: Ich kenne Architekturstyles für die Entwicklung von Container Applikationen | JF1: Ich kann Microservices erklären | JE1: Ich kann Microservices erstellen |
| Kubernetes       | 2.4 | KG1: Ich kenne Kubernetes | KF1: Ich kann Kubernetes erklären | KE1: Ich kann die Merkmale von Kubernetes erklären |
| Kubernetes Installation | 2.4 | KG2: Ich kenne Kubernetes Installationsmöglichkeiten | KF2: Ich kann Kubernetes installieren | KE2: Ich kann einen Kubernetes Cluster installieren |
| Kubernetes Ressourcen | 2.4 | KG3: Ich kenne Kubernetes Ressourcen | KF3: Ich kann Kubernetes Ressourcen erklären | KE3: Ich kann Kubernetes Ressourcen deren Einsatzzweck zuordnen |
| Continuous Integration / Delivery | 2.5 | LG1: Ich kenne Continuous Integration / Delivery | LF1: Ich kann Continuous Integration / Delivery erklären | LE1: Ich kann Continuous Integration / Delivery einsetzen |

## Kompetenzstufen

### Grundlagen | Stufe 1 

Diese Stufe ist als Einstieg ins Thema gedacht. Der Fokus liegt hier auf dem Verstehen von Begriffen und Zusammenhängen. 

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 3.0.*

### Fortgeschritten | Stufe 2 

Diese Stufe definiert den Pflichtstoff, den alle Lernenden am Ende des Moduls möglichst beherrschen sollen.  

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 4.5*

### Erweitert | Stufe 3 

Diese Lerninhalte für Lernende gedacht, die schneller vorankommen und einen zusätzlichen Lernanreiz erhalten sollen.  

*Als Richtungshinweis: Wer alle Kompetenzen in dieser Stufe erfüllt, hat die Noten 6*

## Fragekatalog

Link zum [Fragekatalog](Fragenkatalog.md)
