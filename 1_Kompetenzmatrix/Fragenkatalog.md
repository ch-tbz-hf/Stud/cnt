## Fragenkatalog

[[_TOC_]]

### Cloud Computing

Was versteht man unter Cloud-Computing?

<details><summary>Antwort</summary>
<p>
Darunter versteht man die Ausführung von Programmen, die nicht auf dem lokalen Rechner installiert sind, sondern auf einem anderen Rechner, der aus der Ferne aufgerufen wird (bspw. über das Internet).
</p>
</details>

---

Was versteht man unter Infrastructure as a Service - IaaS?

<details><summary>Antwort</summary>
<p>
Die Infrastruktur stellt die unterste Schicht im Cloud Computing dar. Der Benutzer greift hier auf bestehende Dienste innerhalb des Systems zu, verwaltet seine Recheninstanzen (virtuelle Maschinen) allerdings weitestgehend selbst.
</p>
</details>

---

### Infrastructure as Code

Was ist der Unterschied zur manuellen Installation der VM
<details><summary>Antwort</summary>
<p>
    Automation, Wiederholbarkeit, Dokumentation 
</p>
</details>

---

### Service Discovery

Was ist der Unterschied zwischen einem Web Server und einen Reverse Proxy?
<details><summary>Antwort</summary><p>    
     Web Server handelt HTML Seiten direkt ab, Reverse Proxy dient als Stellvertretter für einen Web Server o.ä.    
</p></details>

---

### ssh

Was ist der Unterschied zwischen der id_rsa und id_rsa.pub Datei?
<details><summary>Antwort</summary><p>    
     Private und Public Key     
</p></details>

---

Wo darf ein SSH Tunnel nicht angewendet werden?
<details><summary>Antwort</summary><p>    
     In der Firma   
</p></details>

---

Für was dient die Datei `authorized_keys`?
<details><summary>Antwort</summary><p>    
     Beinhaltet die Public Key von allen wo ohne Password auf System dürfen
</p></details>

--- 

Für was dient die Datei `known_hosts`?
<details><summary>Antwort</summary><p>    
     Liste der Systeme wo ich mich via ssh Verbunden habe - steht nicht in Doku -> Googeln  
</p></details>

---

### Container


Was ist der Unterschied zwischen Virtuellen Maschine und Docker?
<details><summary>Antwort</summary>
    VM steht für IaaS (Virtuelle Maschinen) und Docker für PaaS bzw. CaaS (Container)
</p></details>

---

Welche Linux Kernel Technologie verwenden Container?
<details><summary>Antwort</summary>  
        Linux Namespaces, siehe auch [The Missing Introduction To Containerization](https://medium.com/faun/the-missing-introduction-to-containerization-de1fbb73efc5)
</p></details>

---

Welches Architekturmuster verwendet der Entwickler wenn er Container einsetzt?
<details><summary>Antwort</summary>  
        Microservices
</p></details>

---

Welches sind die drei Hauptmerkmale (abgeleitet vom Ur-Unix) von Microservices?
<details><summary>Antwort</summary>  
        Ein Programm soll nur eine Aufgabe erledigen, und das soll es gut machen. Programme sollen zusammenarbeiten können. Nutze eine universelle Schnittstelle. In UNIX sind das Textströme. Bei Microservices das Internet (REST).
</p></details>

---

### Container


Was ist der Unterschied zwischen einem Container Image und einem Container?
<details><summary>Antwort</summary>
        Image = gebuildet und readonly, Container Image + aktuelle Änderungen im Filesystem
</p></details>

---

Was ist der Unterschied zwischen einer Virtuellen Maschine und einem Container?
<details><summary>Antwort</summary>
        VM hat Betriebssystem mit am laufen, Docker nur die eigenen Prozesse
</p></details>
    
---

Was ist der Unterschied zwischen einer Container Registry und einem Repository
<details><summary>Antwort</summary>
        In der Registry werden die Container Images gespeichert. Ein Repository speichert pro Container Image verschiedene Versionen von Images.
</p></details>

---

Wie erstelle ich ein Container Image
<details><summary>Antwort</summary>
    docker build 
</p></details>

---

In welcher Datei steht welche Inhalte sich im Container Image befinden?
<details><summary>Antwort</summary>
    Dockerfile 
</p></details>

---

Der erste Prozess im Container bekommt die Nummer?
<details><summary>Antwort</summary>
    1 
</p></details>

---

Welche Teile von Docker sind durch Kubernetes obsolet geworden, bzw. sollten nicht mehr verwendet werden?
<details><summary>Antwort</summary>
    Swarm, Compose, Network, Volumes
</p></details>

---

Welche Aussage ist besser (siehe auch [The Twelve-Factor App](https://12factor.net/))?
* a) Dockerfile sollten möglichst das Builden (CI) und Ausführen von Services beinhalten, so ist alles an einem Ort und der Entwickler kann alles erledigen.
* b) Das Builden und Ausführen von Services ist strikt zu trennen. Damit saubere und nachvollziehbare Services mittels CI/CD Prozess entstehen. 
<details><summary>Antwort</summary>
    b)
</p></details>

---

### Registry


Was ist Docker Hub?
<details><summary>Antwort</summary>
        Ein Container Registry, wo Container Image gespeichert werden. Docker Hub wird durch die Firma Docker zur Verfügung gestellt wird.
</p></details>

---

Welches sind die Alternativen?
<details><summary>Antwort</summary>
        Praktisch jeder Cloud Anbieter stellt eine Container Registry zur Verfügung. Auch die Anbieter für die Verwaltung von Build Artefakten (z.B. Sonatype Nexus) stellen Docker Registries zur Verfügung oder haben deren Funktionalität integriert. 
</p></details>

---

Warum sollte eine eigene Docker Registry im Unternehmen verwendet werden?
<details><summary>Antwort</summary>
        Sicherheit, bzw. das mögliche Fehlen davon. Es kann nicht Sichergestellt werden, dass alle Container Images auf Docker Hub sicher sind.
</p></details>

---

Warum sollten Versionen `tag` von Images immer angegeben werden?
<details><summary>Antwort</summary>
        Ansonsten wird `latest` verwendet und so nicht sicher welche Version wirklich verwendet wird.
</p></details>

---

### Kubernetes


Was ist Kubernetes?
<details><summary>Antwort</summary><p>    
     Das im Juli 2014 gestartete Kubernetes (griechisch: Steuermann) stellt die derzeit populärste Container-Cluster-/Orchestrierungs-Lösung dar.
</p></details>

---

Was ist die Hauptaufgabe von Kubernetes?
<details><summary>Antwort</summary><p>    
     Kubernetes Hauptaufgabe ist die Verwaltung und Orchestrierung der Container innerhalb eines Clusters, der üblicherweise aus mindestens einem Kubernetes Master und multiplen Worker Nodes besteht.
</p></details>

---

Wer ist der Eigentümer von Kubernetes?
<details><summary>Antwort</summary><p>    
     Kubernetes ist mittlerweile bei der Cloud Native Computing Foundation (http://cncf.io) gehostet.
</p></details>

---

Was für eine Netzwerkstruktur verwendet Kubernetes?
<details><summary>Antwort</summary><p>    
     Kubernetes verwendet im Unterschied zu Docker eine flache Netzwerkstruktur. 
* Jeder Container kann mit jedem anderen ohne NAT kommunizieren.
* Alle Kubernetes Nodes können mit allen Containern (und in die andere Richtung) ohne NAT kommunizieren.
* Die IP, die ein Container von sich selbst sieht, ist auch die, die jeder andere Node oder Container im Netz von ihm sieht.
</p></details>

---

Über was Kommunizieren die Services von Nodes zu Nodes
<details><summary>Antwort</summary><p>    
     Über ein Overlay Netzwerk, welches auf jeder Node ein Subnetz zur Verfügung stellt.
</p></details>

---


### Objekte (Ressourcen)


Kubernetes Objekte (Ressourcen) werden im welchen Dateiformat beschrieben?
<details><summary>Antwort</summary><p>    
     YAML
</p></details>

---

Kubernetes Objekte (Ressourcen) können mittels Dashboard und welche CLI Tool verwaltet werden?
<details><summary>Antwort</summary><p>    
     kubectl
</p></details>

---

Mit was lassen sich Kubernetes Objekte (Ressourcen) gruppieren?
<details><summary>Antwort</summary><p>    
     Labels
</p></details>

---

Was sind Pods?
<details><summary>Antwort</summary><p>    
     Kleine Gruppe von Containern welche eng verbunden sind. Kleinste Einheit für Replikation und Platzierung (auf Node). “Logischer” Host für Container. Jeder Pods erhält genau eine IP-Adresse
</p></details>

---

Was sind Services?
<details><summary>Antwort</summary><p>    
     Eine Gruppe von Pods die zusammenarbeiten, Gruppiert mittels Label Selector. Erlaubt mittels unterschiedlichen Methoden auf den Service zuzugreifen, z.B. DNS Name. Definieren Zugriffsrichtlinien, z.B. Port Remapping für den Zugriff von ausserhalb des Clusters. 
</p></details>

---

Was ein Ingress?
<details><summary>Antwort</summary><p>    
    Ein API-Objekt, das den externen Zugriff auf die Dienste in einem Cluster verwaltet, in der Regel mittels HTTP. 
    Grob entspricht der Ingress Dienst dem Reverse Proxy Muster. 
</p></details>

---

Was sind Namespaces bzw. deren Aufgabe?
<details><summary>Antwort</summary><p>  
    Sie Unterteilen den gesamten K8s Cluster in logische Partitionen bzw. Bereiche. Vergleichbar mit Subdomains.
</p></details>

---

Was ist die Aufgabe eines ReplicaSets?
<details><summary>Antwort</summary><p>  
Stellt sicher, dass N Pods laufen sind es zu wenig, werden neue gestartet, sind es zu viele werden Pods beendet, gruppiert durch den Label Selector
</p></details>

---

Für was können Deployments verwendet werden?
<details><summary>Antwort</summary><p>  
Ermöglichen Deklarative Updates von Container Images in Pods. 
</p></details>

---

