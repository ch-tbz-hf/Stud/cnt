![TBZ Logo](./x_gitressourcen/tbz_logo.png)  

# CNT Cloud Native Technology

* [Kompetenzmatrix](1_Kompetenzmatrix)
* [Umsetzung / Modulablauf](3_Umsetzung)
* [Unterrichtsressourcen](2_Unterrichtsressourcen)   

    0. Grundlagen (Vorausssetzungen)
        * [Linux Essentials - Die Einsteiger-Zertifizierung des LPI](https://learning.lpi.org/de/learning-materials/010-160/)
        * [Linux-Administration II Linux im Netz](https://www.tuxcademy.org/product/adm2/), Kapitel 10 - Die Secure Shell
        * [Debian-Paketverwaltung verwenden](https://learning.lpi.org/de/learning-materials/101-500/102/102.4/) 
        * Informatik- und Netzinfrastruktur für ein kleines Unternehmen realisieren und zwei Netzwerk verbinden. Module [117](https://gitlab.com/ch-tbz-it/Stud/m117/) und [145](https://gitlab.com/alptbz/m145/)
    1. MAAS – Metal as a Service und Cloud
        * **A** - [Virtualisierung und Virtuelle Maschinen](2_Unterrichtsressourcen/A/)
        * **B** - [Infrastructure as Code](2_Unterrichtsressourcen/B/)
        * **C** - [Cloud - Funktionsweise, Servicemodelle](2_Unterrichtsressourcen/C/)
        * **D** - [Private Cloud Einrichten](2_Unterrichtsressourcen/D/)
        * **E** - [Service Discovery (Portmapping, Reverse Proxy)](2_Unterrichtsressourcen/E/)
        * **F** - [Persistenz](2_Unterrichtsressourcen/F/)
        * **G** - [Terraform - Infrastruktur als Code- Software-Tool](2_Unterrichtsressourcen/G/)        
    2. Container und Kubernetes, DevOps
        * **H** - [Container Standards und Technologien](2_Unterrichtsressourcen/H/)
        * **I** - [Container Umgebungen, Images und Registry](2_Unterrichtsressourcen/I/)
        * **J** - [Architektur für Container/Kubernetes (Microservices)](2_Unterrichtsressourcen/J/)
        * **K** - [Kubernetes](2_Unterrichtsressourcen/K/)
        * **L** - [Continuous Integration / Delivery](2_Unterrichtsressourcen/L/)
* [Allgemeines](#allgemeines)

## Client Umgebung

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/ch-tbz-hf/Stud/cnt)

## Kurzbeschreibung des Moduls 

### Teil 1 MAAS – Metal as a Service – wir bauen eine Cloud

Ausgehend von automatisierten, auf Self-Service-Technologien basierenden Remoteinstallationen von Be-triebssystemen werden Grundlagen, Technologien und Praxis zum Bau einer Cloud-Lösung vermittelt. Sie lernen in diesem Modul, wie physische Rechner schnell und automatisiert auf Computerhardware installiert werden können und wie dies über einen Service effizient betrieben werden kann. Dazu werden notwendige Aktualisierungen des dazu notwendigen Vorwissens in Netzwerk und Computertechnik bearbeitet. 

### Teil 2 Container und Kubernetes – wir bauen einen Kubernetes Cluster

Einführung in den Betrieb von Containern und von Kubernetes. Erstes Kennenlernen und Einsatz von Produkten aus dem Cloud Native Computing Foundation (CNCF) Oekosystem (CNCF Trail Map, Container Runtime & Registry, CI/CD, Kubernetes, Orchestration & Application Declaration, Service Proxy & Discov-ery, Service Distribution).

## Kompetenzmatrix

Kompetenzmatrix zu den Handlungszielen in drei Abstufungen [Link](1_Kompetenzmatrix)

## Umsetzung / Modulablauf

Umsetzung mit Modulablauf [Link](3_Umsetzung)

## Unterrichtsressourcen

Unterrichtsressourcen von Aufträgen und Inhalten zu den einzelnen Kompetenzen [Link](2_Unterrichtsressourcen)

## Allgemeines

* **Präsenzlektionen**: 80
* **Selbststudium**: 20
* **Modulstufe**: Grundlagen
* **Bereich**: Fachspezifisches
* **Voraussetzungen**: 
    * Ich kann eine Informatik- und Netzinfrastruktur für ein kleines Unternehmen realisieren und zwei Netzwerke verbinden. Module [117](https://gitlab.com/ch-tbz-it/Stud/m117/) und [145](https://gitlab.com/alptbz/m145/)
    * [Linux Essentials - Die Einsteiger-Zertifizierung des LPI](https://www.tuxcademy.org/product/lxes/)
    * Ich kann Abläufe mit [Bash](https://github.com/mc-b/M300/blob/master/80-Ergaenzungen/linux/10-Bash.md) Scripts in der Systemadministration automatisieren. Modul [122](https://gitlab.com/ch-tbz-it/Stud/m122)
* **Technik/Methoden**: Theorie- Unterricht, Einzel- und Gruppenarbeit, Übungen (Hands-on)
* **Schlüsselbegriffe**: Container, Cluster, Cloud, DevOps
* **Lernzielkontrolle**: 2 Prüfungen, Transferarbeiten
* **Abschlussqualifikation**: erfüllt
* **Dispensation**: keine
* **Lehr- und Lernformen**: Lehrervortrag, Lehrgespräch, handlungsorientierter, interaktiver Unterricht mit Einzel- und Gruppenarbeiten, Präsentationen
* **Lehrmittel**: eigenes Script,  Produktedokumentationen im Internet (u.a. maas.io, kubernetes.io)


- - - 

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/ch/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Schweiz Lizenz</a>.

- - - 

* Autor: Marcel Bernet 
* Mail: ![](x_gitressourcen/mailto.png)
