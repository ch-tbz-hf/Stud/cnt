MultiPass und Cloud-init
------------------------

Multipass kann, auf dem lokalen Notebook, virtuelle Maschinen starten und ausführen und sie mit Cloud-Init wie eine Public Cloud konfigurieren. 

Damit eignet sich Multipass als Alternative zum MAAS oder der Public Cloud.

### Hands-on

Als erstes ist dieses Repository zu clonen

    git clone https://gitlab.com/ch-tbz-hf/Stud/cnt.git
    
#### Kapitel [B Infrastruktur als Code](B/)  

Einfache VMs erstellen:

    cd cnt/2_Unterrichtsressourcen/B/
    multipass launch --name php --cloud-init cloud-init-php.yaml
    multipass launch --name mysql --cloud-init cloud-init-mysql.yaml
    multipass set client.primary-name=php  
    
Ausgabe der IP-Adressen

    multipass list
    
Anwahl der PHP VM im Browser und gleich Verfahren wie in [B/cloud-init-php.md] beschrieben.

#### Kapitel [D Private Cloud oder Metal as a Service (MAAS)](D/)

![](https://assets.ubuntu.com/v1/6e132859-MAAS+tutorial+diagram-02.svg)

Quelle: maas.io

- - -

Bei genüged [Ressourcen](https://maas.io/tutorials/build-a-maas-and-lxd-environment-in-30-minutes-with-multipass-on-ubuntu#2-requirements) lässt sich die MAAS Umgebung auch lokal auf dem Notebook aufsetzen.

Dazu ist der Dokumentation [hier](https://maas.io/tutorials/build-a-maas-and-lxd-environment-in-30-minutes-with-multipass-on-ubuntu#1-overview) zu folgen.

Zuerst sind die Netzwerk Adapter des Notebooks aufzulisten und der Netzwerk Adapter auszulesen, welcher mit dem Internet verbunden ist

    multipass networks
    
Ausgabe    
   
    Name             Type      Description
    Default Switch   switch    Virtual Switch with internal networking
    Default          ethernet  Realtek PCIe GbE Family Controller
    
Bei dieser Liste wäre es Default. Dieser ist nach unten zu übertragen, beim Eintrag `--network`,

    cd cnt/2_Unterrichtsressourcen/D/
    # wget  https://raw.githubusercontent.com/canonical/maas-multipass/main/maas.yml -O maas.yaml
    multipass launch --name maas -c4 -m8GB -d64GB --network Default --cloud-init maas.yaml

Bevor eine VM im MAAS erstellt werden kann, ist **unter Windows**, VM in VM zu aktivieren. Dazu ist zuerst die VM zu stoppen und anschliessend, in der PowerShell mit Administratorenrechten, folgende Befehle auszuführen:

    multipass stop maas
    Set-VMProcessor -VMName maas -ExposeVirtualizationExtensions $true
    multipass start maas
    
Ausgabe IP-Adresse VM und und den Assistenten [hier](https://maas.io/tutorials/build-a-maas-and-lxd-environment-in-30-minutes-with-multipass-on-ubuntu#6-log-into-maas) im Browser abarbeiten:    

    multipass list
    http://<MAAS_IP>:5240/MAAS      # im Browser öffnen
   
Anschliessend können virtuelle Maschinen wie [hier](https://maas.io/tutorials/build-a-maas-and-lxd-environment-in-30-minutes-with-multipass-on-ubuntu#8-create-a-vm-guest) beschrieben erstellt werden. 

#### Kapitel [E - Service Discovery](E/)       
    
Aufsetzen des Webshops 

    cd cnt/2_Unterrichtsressourcen/E/
    multipass launch --name order --cloud-init order.yaml
    multipass launch --name customer --cloud-init customer.yaml
    multipass launch --name catalog --cloud-init catalog.yaml
    multipass launch --name apache --cloud-init apache.yaml
    multipass set client.primary-name=apache   
    
Anwahl der Apache VM im Browser, weiter bei [Reverse Proxy](E#reverse-proxy). 

#### Kapitel [H - Container Standards und Technologien](H/)

Das Multipass Team hat eine [Docker Umgebung](https://ubuntu.com/blog/docker-on-mac-and-windows-multipass) veröffentlicht. Mit dieser wird eine voll funktionsfähige Docker Umgebung gestartet.

    multipass launch docker
    
Um die Benützung noch einfacher zu Gestalten, kann ein Alias eingerichtet werden. Dann ist es möglich die Docker Befehle auf dem lokalen Notebook auszuführen. Effektiv werden diese aber in die erstellte VM weitergeleitet. Dabei ist der nachfolgende Befehl einzugeben und den Instruktionen zu folgen:

    multipass alias docker:docker   
    
* [Running a container with the Docker](https://multipass.run/docs/docker-tutorial)
* [DockerHub download rate limits](https://microk8s.io/docs/dockerhub-limits)
    
#### Kapitel [K - Kubernetes](K/)

    cd cnt/2_Unterrichtsressourcen/K/
    multipass launch --name k8smaster --mem 4G --disk 32G --cpus 2 --cloud-init cloud-init.yaml
    multipass set client.primary-name=k8smaster

Jupyter Verzeichnis in VM als /data mounten

    multipass set local.privileged-mounts=true
    multipass mount jupyter k8smaster:/data

Hinweise: 
* Mounten gibt Probleme bei `multipass suspend k8smaster` und anschliessend `multipass start k8smaster`.
* Bei Verwendung von `multipass start/stop` wird die IP-Adresse geändert. Dann funktioniert kubectl nicht mehr. Lösung `~/.kube/config` IP-Adresse auf `127.0.0.1` ändern.

### Links

* [Dokumentation Multipass](https://multipass.run/docs)
* [How to create an instance](https://multipass.run/docs/create-an-instance)
