## Private Cloud oder Metal as a Service (MAAS)

[[_TOC_]]

![](https://github.com/mc-b/lernmaas/raw/master/doc/images/howitworks.png)

Quelle: maas.io

- - - 

Canonical (Ubuntu) MAAS bittet schnelle Serverbereitstellung für Ihr Rechenzentrum.

Dabei MAAS verfügt über eine abgestufte Architektur
* Ein 'Region Controller (regiond)' nimmt die Bereitstellungsanforderungen via UI oder REST-API entgegen.
* Rack Controller (Rackd) verwalten die Bare-Metal Server wo die virtuellen Maschinen betrieben werden.

**Zusammengefasst**: [MAAS](https://maas.io/) verwandelt Ihr Rechenzentrum in eine Bare-Metal-Cloud.

### Einführung 

![](https://pages.ubuntu.com/rs/066-EOV-335/images/MAAS.png)

- - -

* [Webinar](https://pages.ubuntu.com/Register_Cloud-Ready_Servers_in_minutes.html)

### Installation

![https://player.vimeo.com/video/207284301?h=8244594d18](https://player.vimeo.com/video/207284301?h=8244594d18)

### Hands-on

* [Installation MAAS](maas-install.md)
* [Installation VPN](vpn-install.md)
