Installation MAAS
-----------------

[[_TOC_]]

### Master
 
[Ubuntu](https://ubuntu.com/download/desktop) als Desktop aufsetzen. Dabei genügt die minimale Version. 

Fixe IP-Adresse vergeben, z.B. über Einstellungen, Software Update durchführen und MAAS Installieren

    sudo apt-add-repository ppa:maas/3.1
    sudo apt update
    sudo apt upgrade -y
    sudo apt install -y maas jq markdown nmap traceroute git curl wget openssh-server 

MAAS Admin User erstellen. Als Username `ubuntu` verwenden. 

    sudo maas createadmin 
    
MAAS Admin User Name `ubuntu` als Umgebungvariable setzen. Erlaubt einen einfacheren Zugriff via CLI.

    cat <<%EOF% >>$HOME/.bashrc
    export PROFILE=ubuntu
    %EOF%
    
SSH-Key erstellen, den brauchen wir nachher

    ssh-keygen    
    
Browser starten und UI von MAAS aufrufen [http://localhost:5240](http://localhost:5240)

* SSH-Key, von vorher `cat ~/.ssh/id_rsa.pub`  eintragen
* Den MAAS Master (braucht es für die interne Namensauflösung) und die bekannten DNS Server eintragen
* Bei Subnets DHCP Server aktivieren auf z.B. 172.16.17.x, Gateway IP: 172.16.17.1 und DNS Server (z.B. OpenDNS 208.67.222.222, 208.67.220.22) eintragen

**Server frisch starten, ansonsten werden die Änderungen nicht übernommen.**

### Worker Nodes   

Die Worker Nodes sind so zu Konfigurieren, dass sie via Netzwerk (PXE Boot) booten.

Anschliessend sind die zwei Installationsroutinen durchzuführen. 

- - -

[![](https://img.youtube.com/vi/jj1M-YyCgD4/0.jpg)](https://www.youtube.com/watch?v=jj1M-YyCgD4)

MAAS Enlistment (auf Bild klicken, damit YouTube Film startet)

---

[![](https://img.youtube.com/vi/k-9VHZg_qoo/0.jpg)](https://www.youtube.com/watch?v=k-9VHZg_qoo)

MAAS Commission (auf Bild klicken, damit YouTube Film startet)

- - -

Die neue Maschine anklicken und rechts oben mittels `Take action` -> Deploy die Software deployen (Ubuntu 20.04). Um auf der Maschine nachher virtuelle Maschinen erstellen zu können ist die Checkbox `Register as MAAS KVM host` zu aktivieren.

Nach der Installation steht die Maschine unter KVM zur Verfügung und es lassen sich neue virtuelle Maschinen darauf erstellen (Compose).

**Tips** 
* Normale PCs haben keine Unterstützung für [BMC](https://de.wikipedia.org/wiki/Baseboard_Management_Controller) deshalb muss der `Power type` auf `Manuel` eingestellt werden. Sollte das nicht funktionieren, zuerst bei `Configuration` unter `Power type`  IPMI und eine Pseudo IP und MAC Adresse eingeben und nachher auf `Manuel` wechseln.
* TBZ PCs haben integriertes Intel AMT. Power On, `Ctrl-P`, Default Password `admin` durch internes Ersetzen, IP Einstellungen vornehmen (ich verwende fixe IP ist mehr Aufwand aber nachher einfacher und **TCP Aktivieren**. Probieren mit `http://IP-Adresse:16992`. Vorsicht das AMT UI verwendet fix den US Tastaturlayout.
* PCs mittels `Lock` vor unbeabsichtigtem Ändern schützen.

### MAAS CLI und Tests

Einlogen für CLI Access (die Umgebungensvariable haben wir oben gesetzt!)

    maas login ${PROFILE} http://localhost:5240/MAAS/api/2.0
    
Der verlangte API Key finden wir im UI von MAAS unter `ubuntu` -> `API-Keys`.    
    
Mögliche Befehle anzeigen

    maas ${PROFILE} --help
    
Erstelle Maschinen im JSON Format ausgeben:

    maas $PROFILE machines read
    
Es sollten die KVM Maschinen angezeigt werden.    
  
## Links

* [Bare Metal to Kubernetes-as-a-Service - Part 1](https://www.2stacks.net/blog/bare-metal-to-kubernetes-part-1/)
* [MAAS Blog Übersicht](https://ubuntu.com/blog/tag/maas)
