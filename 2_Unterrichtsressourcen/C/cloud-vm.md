## Cloud Accounts und VMs anlegen

[[_TOC_]]

### Erstellt einen (free) Account auf einer der folgenden Cloud Plattformen:

* [Azure](https://azure.microsoft.com/en-us/free/)
* [AWS](https://aws.amazon.com/de/free/)
* [Google](https://cloud.google.com/free)

Macht Euch mit dem Anlegen von VMs auf den Cloud Plattformen vertraut.

***
### VMs Anlegen

Macht Euch mit dem Anlegen einer virtuellen Maschine, mittels dem jeweiligen User Interface, vertraut. Als Betriebsystem verwendet Ihr **Ubuntu** Linux.

Nachdem Ihr sicher seit, wie das Anlegen funktioniert, könnt Ihr die VM wieder löschen.

**Für die weiteren Arbeiten ist es Vorraussetzung, dass Ihr selbstständig eine virtuelle Maschine anlegen könnt.**

#### Azure Cloud

![](../x_gitressourcen/azure-create-vm.png)

* [Schritt für Schritt Anleitung](https://docs.microsoft.com/en-us/azure/virtual-machines/linux/quick-create-portal)
* [Create VM Wizard](https://portal.azure.com/#create/Canonical.UbuntuServer1804LTS-ARM)

#### AWS Cloud

![](../x_gitressourcen/aws-create-vm.png)

* [Schritt für Schritt Anleitung](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EC2_GetStarted.html)
* [Create WM Wizard](https://eu-central-1.console.aws.amazon.com/ec2/v2/home?region=eu-central-1#LaunchInstanceWizard:)

***
#### Cloud CLIs Einrichten

Installiert, dass zur Cloud, gehörende Kommandozeilentool (CLI) auf Eurem lokalen Notebook/PC.

* [Azure](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli)
* [AWS](https://aws.amazon.com/de/cli/)

**Achtung**: für AWS sind weitere [Konfigurationen](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html) nötig.

