## Cloud

[[_TOC_]]

### Eisenzeit (Imperativ) vs. die Cloud (Deklarativ)

In der **Eisenzeit** der IT waren Systeme direkt an physische Hardware gebunden. 
Das Bereitstellen und Warten der Infrastruktur war manuelle Arbeit, die die Menschen dazu zwang, zu klicken und zu tippen, um die Systeme am laufen zu halten. 
Da Änderungen viel Zeit und Geld erforderten, wurde in Änderungsmanagement und sorgfältige Prüfung, investiert
Dies machte Sinn, weil es teuer war, etwas falsch zu machen.

Im **Cloud-Zeitalter** der IT werden Systeme von der physischen Hardware entkoppelt.
Die routinemässige Bereitstellung und Wartung kann an Softwaresysteme delegiert werden, um den Menschen von Routinearbeiten zu befreien. 
Änderungen können in Minuten, wenn nicht Sekunden vorgenommen werden. 
Wir können diese Geschwindigkeit nutzen für eine höhere Zuverlässigkeit sowie schnellere Einführung von neuen Produkten. 

### Funktionsweise

[![](https://img.youtube.com/vi/KXkBZCe699A/0.jpg)](https://www.youtube.com/watch?v=KXkBZCe699A)

### Servicemodelle

[![](https://img.youtube.com/vi/4fvWPc1AsUs/0.jpg)](https://www.youtube.com/watch?v=4fvWPc1AsUs)

### Lift & Shift

Lift & Shift (u.a. ReHosting) verschiebt Anwendungen und Daten in die Cloud, ohne dass grössere Veränderungen an der Funktionsweise, dem Design und dem Code der Applikation vorgenommen werden.

Lift & Shift nutzt in der Regel das Cloud-Computing-Servicemodell IaaS (Infrastructure as a Service). Auf dieser Plattform sind die Applikationen aus der On-Premises-Umgebung in abstrahierter und virtualisierter Form nahezu unverändert weiter betreibbar.

Vorteil: ein Unternehmen kann den Wechsel auf eine Cloud-Plattform sehr schnell und ohne grossen finanziellen und arbeitstechnischen Einsatz vollziehen.
 
Nachteil: zahlreiche Vorzüge des Cloud Computings (Scalierung, Lastausgleich, Statusüberwachung etc.) durch die unveränderte Übertragung der Applikation in die Cloud nicht zum Tragen kommen.

### Lift & Reshape

Lift & Reshape, erweitert „Lift & Shift“ dahingehend, dass bei der Migration eine Optimierung hinsichtlich der Infrastruktur (z.B. Anzahl der virtuellen CPUs in einer VM oder Update auf eine neuere Betriebssystemversion) vorgenommen wird.

Gegebenenfalls sind kleinere Anpassungen an der Software notwendig oder muss eine Neuinstallation in der Zielplattform durchgeführt werden. In diesem Schritt werden – sofern sinnvoll – auch Datenbanken zu PaaS migriert. In diesem Zuge ist mitunter auch ein Wechsel der Datenbank-Engine möglich, um zukünftig Lizenzkosten zu sparen (z.B. von Oracle nach PostgreSQL)

Ferner versteht man unter Lift & Reshape auch den Umzug einer bestehenden Applikation auf eine andere Plattform. Dies eröffnet neue Möglichkeiten wie Lastverteilung und automatische Skalierung bis zur Statusüberwachung der Anwendung, so dass sich Entwickler um die darunterliegenden Server nicht weiter kümmern müssen.

Quelle: [https://blog.materna.de/cloud-migration/](https://blog.materna.de/cloud-migration/) 

### Hands-on

* [Cloud Accounts und VMs anlegen](cloud-vm.md)  
* [VM mit Services in der Cloud anlegen](cloud-iac.md)
* [VM mit Services, mittels CLI, anlegen](cloud-iac-cli.md)  