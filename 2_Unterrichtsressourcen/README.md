## Unterrichtsressourcen

[[_TOC_]]

## 0. Grundlagen

* [Linux Essentials - Die Einsteiger-Zertifizierung des LPI](https://learning.lpi.org/de/learning-materials/010-160/)
* [Linux-Administration II Linux im Netz](https://www.tuxcademy.org/product/adm2/), Kapitel 10 - Die Secure Shell
* [Debian-Paketverwaltung verwenden](https://learning.lpi.org/de/learning-materials/101-500/102/102.4/)
* Informatik- und Netzinfrastruktur für ein kleines Unternehmen realisieren und zwei Netzwerk verbinden. Module [117](https://gitlab.com/ch-tbz-it/Stud/m117/) und [145](https://gitlab.com/alptbz/m145/)
 

## 1. MAAS – Metal as a Service und Cloud

* **A** - [Virtualisierung und Virtuelle Maschinen](A/)
* **B** - [Infrastructure as Code](B/)
* **C** - [Cloud - Funktionsweise, Servicemodelle](C/)
* **D** - [Private Cloud Einrichten](D/)
* **E** - [Service Discovery (Portmapping, Reverse Proxy)](E/)
* **F** - [Persistenz](F/)
* **G** - [Terraform - Infrastruktur als Code- Software-Tool](G/)

## 2. Container und Kubernetes, DevOps

* **H** - [Container Standards und Technologien](H/)
* **I** - [Container Umgebungen, Images und Registry](I/)
* **J** - [Architektur für Container/Kubernetes (Microservices)](J/)
* **K** - [Kubernetes](K/)
* **L** - [Continuous Integration / Delivery](L/)

## 3. Ergänzende Unterlagen

* [Cloud Native Glossary](https://glossary.cncf.io/)
* [Multipass](Multipass.md)

**Linux**

* [tuxacademy Unterlagen](https://www.tuxcademy.org/media/all/)
* [Linux](https://github.com/mc-b/M300/blob/master/80-Ergaenzungen/linux/01-Linux.md)
* [Bash](https://github.com/mc-b/M300/blob/master/80-Ergaenzungen/linux/10-Bash.md)
* [Advanced Packaging Tool (APT)](https://github.com/mc-b/M300/blob/master/80-Ergaenzungen/linux/20-APTTool.md) 
* [Übersichtsseite zu APT](https://wiki.ubuntuusers.de/APT/)
* [Packetverwaltung Tipps](https://wiki.ubuntuusers.de/Paketverwaltung/Tipps/)
* [Public-Private-Key-Verschlüsselung](https://www.inside-it.ch/de/post/was-ist-eigentlich-20201102) und [ssh](https://wiki.ubuntuusers.de/SSH/)
* [ssh in 6 Minuten](https://www.youtube.com/watch?v=v45p_kJV9i4)
* [Bitvise und ssh](https://www.youtube.com/watch?v=lJjEhWC0XYQ)
* [cURL](https://github.com/mc-b/M300/blob/master/80-Ergaenzungen/linux/30-cURL.md)

**Software Konfiguration**
* [Einleitung](https://github.com/mc-b/M300/blob/master/80-Ergaenzungen/swkonfiguration/01-Einleitung.md)
* [Tools](https://github.com/mc-b/M300/blob/master/80-Ergaenzungen/swkonfiguration/02-Tools.md)
    
**Service Konfiguration**
* [Einleitung](https://github.com/mc-b/M300/blob/master/80-Ergaenzungen/srvkonfiguration/01-Einleitung.md)
* [systemd](https://github.com/mc-b/M300/blob/master/80-Ergaenzungen/srvkonfiguration/02-systemd.md)
