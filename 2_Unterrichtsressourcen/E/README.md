## Service Discovery

[[_TOC_]]

## Grundlagen

### TCP/IP (Internetprotokollfamilie)

![](https://s3-us-west-2.amazonaws.com/mbed-os-docs-images/ip-networking.png)

Quelle: [mbed.com](https://os.mbed.com/docs/mbed-os/latest/apis/network-socket.html)
- - -

Transmission Control Protocol / Internet Protocol (TCP/IP) ist eine Familie von Netzwerkprotokollen und wird wegen ihrer großen Bedeutung für das Internet auch als Internetprotokollfamilie bezeichnet.

Ursprünglich wurde TCP als monolithisches Netzwerkprotokoll entwickelt, jedoch später in die Protokolle IP und TCP aufgeteilt. Die Kerngruppe der Protokollfamilie wird durch das User Datagram Protocol (UDP) als weiteres Transportprotokoll ergänzt. Außerdem gibt es zahlreiche Hilfs- und Anwendungsprotokolle, wie zum Beispiel [DHCP](http://de.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol) und [ARP](http://de.wikipedia.org/wiki/Address_Resolution_Protocol).

Die Identifizierung der am Netzwerk teilnehmenden Geräte geschieht über [IP-Adressen](http://de.wikipedia.org/wiki/IP-Adresse) (z.B. 10.10.32.1).

### IP-Adresse 

Netzwerke haben unterschiedliche Netzwerkadressen und jedes Geräte (Computer, mbed Board etc.) im Netzwerk eine eigene IP-Adresse.

Eine [IP-Adresse](https://de.wikipedia.org/wiki/IP-Adresse) ist eine Adresse in Computernetzen, die – wie das Internet – auf dem Internetprotokoll (IP) basiert. Sie wird Geräten zugewiesen, die an das Netz angebunden sind, und macht die Geräte so adressierbar und damit erreichbar. Die IP-Adresse kann einen einzelnen Empfänger oder eine Gruppe von Empfängern bezeichnen (Multicast, Broadcast). Umgekehrt kann einem Gerät mehrere IP-Adressen zugeordnet werden.

Die IP-Adresse wird verwendet, um Daten von ihrem Absender zum vorgesehenen Empfänger transportieren zu können. Ähnlich der Postanschrift auf einem Briefumschlag werden Datenpakete mit einer IP-Adresse versehen, die den Empfänger eindeutig identifiziert. Aufgrund dieser Adresse können die „Poststellen“, die [Router](https://de.wikipedia.org/wiki/Router), entscheiden, in welche Richtung das Paket weitertransportiert werden soll. Im Gegensatz zu Postadressen sind IP-Adressen nicht an einen bestimmten Ort gebunden.

Die bekannteste Notation der heute geläufigen IPv4-Adressen besteht aus vier Zahlen, die Werte von 0 bis 255 annehmen können und mit einem Punkt getrennt werden, beispielsweise **192.0.2.42.** Technisch gesehen ist die Adresse eine 32-stellige (IPv4) oder 128-stellige (IPv6) Binärzahl.

Die IP-Adressen **10.0.0.0 bis 10.255.255.255 und 192.168.0.0 bis 192.168.255.255** sind für den **privaten Bereich reserviert** und werden nicht ins Internet geroutet.

Für die Bildung von Teilnetzen siehe [Netzklassen (veraltet)](https://de.wikipedia.org/wiki/Netzklasse) und [Classless Inter-Domain Routing](https://de.wikipedia.org/wiki/Classless_Inter-Domain_Routing).

### Port

Ein [Port](http://de.wikipedia.org/wiki/Port_(Protokoll)) ist der Teil einer IP-Adresse, der die Zuordnung von TCP- und UDP-Verbindungen und -Datenpaketen zu Server- und Client-Programmen durch Betriebssysteme bewirkt. Zu jeder Verbindung dieser beiden Protokolle gehören zwei Ports, je einer auf Seiten des Clients und des Servers. Gültige Portnummern sind 1-65535 bzw. richtiger 0-65535.

Ports dienen zwei Zwecken:

*   Primär sind Ports ein Merkmal zur Unterscheidung mehrerer Verbindungen zwischen demselben Paar von Endpunkten.
*   Ports können auch Netzwerkprotokolle und entsprechende Netzwerkdienste identifizieren, siehe [/etc/services bzw. C:/Windows/system32/Drivers/etc/services](http://www.penguintutor.com/linux/network-services-ports).

### DHCP 

[Dynamic Host Configuration Protocol (DHCP)](https://de.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol) ermöglicht es, Computer ohne manuelle Konfiguration der Netzwerkschnittstelle in ein bestehendes Netzwerk einzubinden. DHCP verteilt Einstellungen wie IP-Adresse, Netzmaske, Gateway und Name Server (DNS) (je nach Netzwerktyp ggf. noch weitere Einstellungen) an die am Netzwerk angeschlossenen Computer.

### DNS

Das [Domain Name System (DNS)](https://de.wikipedia.org/wiki/Domain_Name_System) ist einer der wichtigsten Internet Dienste. Seine Hauptaufgabe ist die Beantwortung von Anfragen zur Namensauflösung.

Das DNS funktioniert ähnlich wie eine Telefonauskunft. Der Benutzer kennt die Domain (den für Menschen merkbaren Namen eines Rechners im Internet) – zum Beispiel example.org. Diese sendet er als Anfrage in das Internet. Diese Adresse wird dann dort vom DNS in die zugehörige IP-Adresse (die „Anschlussnummer“ im Internet) umgewandelt – zum Beispiel eine IPv4-Adresse der Form 192.0.2.42 oder eine IPv6-Adresse wie 2001:db8:85a3:8d3:1319:8a2e:370:7347, und führt so zum richtigen Gerät.

## Services Discovery

Service Discovery ist der Prozess, Clients eines Service mit Verbindungsinformationen (normalerweise IP-Adresse und
Port) einer passenden Instanz davon zu versorgen.

In einem statischen System auf einem Host ist das Problem einfach zu lösen, denn es gibt nur eine Instanz von allem. 

In einem verteilten System mit mehreren Services, die kommen und gehen, ist das aber viel komplizierter. 

Eine Möglichkeit ist, dass der Client einfach den Service über den Namen anfordert (zum Beispiel db oder api) und im Backend dann ein bisschen Magie geschieht, die dazu die passenden Daten liefert. 

Für unsere Zwecke kann Vernetzung als der Prozess des Verknüpfens von VMs betrachtet werden. 

Es geht nicht darum, reale Ethernet-Kabel einzustecken. VM Vernetzung beginnt mit der Annahme, dass es eine Route zwischen Hosts
gibt – egal, ob diese Route über das öffentliche Internet läuft oder nur über einen schnellen lokalen Switch.

Mit dem Service Discovery können Clients also Services finden, und die Vernetzung kümmert sich darum, die Verbindungen herzustellen. 

Vernetzungs Service-Discovery-Lösungen haben häufig gemeinsame Funktionalität, da Service-Discovery-Lösungen auf Ziele im Netz verweisen und Vernetzungslösungen häufig auch Service-Discovery-Features enthalten.

Weitere Funktionen von Service Discovery können sein:

* Health Checking
* Failover 
* [Load Balancing](https://de.wikipedia.org/wiki/Lastverteilung_%28Informatik%29)
* Verschlüsselung der übertragenen Daten 
* Isolieren von VM Gruppen.

## Beispielapplikation

![](https://github.com/mc-b/duk/raw/e85d53e7765f16833ccfc24672ae044c90cd26c1/data/jupyter/demo/images/Microservices-REST.png)

Quelle: Buch Microservices Rezepte
- - -

Im nächsten Schritt wollen wir obige Applikation zur Verfügung stellen.

Die Applikation besteht aus drei [Microservices](https://microservices.io/): Order, Customer und Catalog.

Order kommuniziert mittels Catalog und Customer via [REST](https://de.wikipedia.org/wiki/Representational_State_Transfer). Ausserdem bietet jeder Microservice einige HTML-Seiten an.

Für jede Microservice erstellen wir eine eigene VM ohne VPN Zugriff. Die VMs bezeichnen wir mit `order`, `customer` und `catalog`.

Dazu brauchen wir drei, einfache Cloud-init Scripts:

#### [Order](order.yaml)

    #cloud-config
    packages:
      - nginx
    write_files:
     - content: |
        <html>
         <body>
          <h1>Order App</h1>
         </body>
        </html>
       path: /var/www/html/index.html
       permissions: '0644'

#### [Customer](customer.yaml)

    #cloud-config
    packages:
      - nginx
    write_files:
     - content: |
        <html>
         <body>
          <h1>Customer App</h1>
         </body>
        </html>
       path: /var/www/html/index.html
       permissions: '0644'
       
#### [Catalog](catalog.yaml)

    #cloud-config
    packages:
      - nginx
    write_files:
     - content: |
        <html>
         <body>
          <h1>Catalog App</h1>
         </body>
        </html>
       path: /var/www/html/index.html
       permissions: '0644'      

## Portmapping - port-based-routing

![](https://jensd.be/wp-content/uploads/nat_beforeafter.png)

Quelle: [Forward a TCP port to another IP or port using NAT with Iptables](https://jensd.be/343/linux/forward-a-tcp-port-to-another-ip-or-port-using-nat-with-iptables)

- - -

Neben der Verwendung von NAT für den Zugriff auf das Internet mit mehreren Computern unter Verwendung einer einzigen IP-Adresse gibt es viele andere Verwendungen von NAT. Eine davon besteht darin, den gesamten Verkehr, der an einen bestimmten TCP-Port gesendet wird, an einen anderen Host weiterzuleiten.

Diese Verhalten wollen wir verwenden um den Datenverkehr von unserem Rack Server an unsere Microservices (ohne VPN Zugriff) weiterzuleiten.

Zuerst müssen wir Port Weiterleitung aktivieren (ist beim Rackserver standardmässig aktiviert).

    sudo sysctl net.ipv4.ip_forward=1
    
Für die korrekte Weiterleitung der Ports brauchen wir alle IP-Adressen der VMs und des Rack Servers, z.B.:

* 10.0.45.8 - Rack Server
* 10.0.45.5 - Order
* 10.0.45.6 - Customer
* 10.0.45.7 - Catalog

mit diesen Informationen können wir nun die Port Weiterleitung einrichten. Dazu braucht es, pro VM, zwei Einträge:

**Order**

    sudo iptables -t nat -A PREROUTING -p tcp --dport 8080 -j DNAT --to-destination 10.0.45.5:80
    sudo iptables -t nat -A POSTROUTING -p tcp -d 10.0.45.5 --dport 80 -j SNAT --to-source 10.0.45.8

**Customer**

    sudo iptables -t nat -A PREROUTING -p tcp --dport 8081 -j DNAT --to-destination 10.0.45.6:80
    sudo iptables -t nat -A POSTROUTING -p tcp -d 10.0.45.6 --dport 80 -j SNAT --to-source 10.0.45.8

**Catalog**

    sudo iptables -t nat -A PREROUTING -p tcp --dport 8082 -j DNAT --to-destination 10.0.45.7:80
    sudo iptables -t nat -A POSTROUTING -p tcp -d 10.0.45.7 --dport 80 -j SNAT --to-source 10.0.45.8
    
Die Einträge können wie folgt kontrolliert werden:

    sudo iptables -t nat -L -n
    
    > Chain PREROUTING (policy ACCEPT)
    > DNAT       tcp  --  0.0.0.0/0            0.0.0.0/0            tcp dpt:8080 to:10.0.45.5:80
    > DNAT       tcp  --  0.0.0.0/0            0.0.0.0/0            tcp dpt:8081 to:10.0.45.6:80
    > DNAT       tcp  --  0.0.0.0/0            0.0.0.0/0            tcp dpt:8082 to:10.0.45.7:80
    > Chain POSTROUTING (policy ACCEPT)
    > SNAT       tcp  --  0.0.0.0/0            10.0.45.5            tcp dpt:80 to:10.0.45.8
    > SNAT       tcp  --  0.0.0.0/0            10.0.45.6            tcp dpt:80 to:10.0.45.8
    > SNAT       tcp  --  0.0.0.0/0            10.0.45.7            tcp dpt:80 to:10.0.45.8
        
    
Nun sind die drei VMs jeweils mittels der VPN-Adresse des Rack Servers und Port 8080 - 8082 erreichbar, z.B.:

    http://<VPN IP Adresse Rack Server>:8080
    
Bei einem Fehler oder wenn die Einträge nicht mehr benötigt werden, können sie wie folgt gelöscht werden:

    sudo iptables -P INPUT ACCEPT
    sudo iptables -P FORWARD ACCEPT
    sudo iptables -P OUTPUT ACCEPT
    sudo iptables -t nat -F
    sudo iptables -t mangle -F
    sudo iptables -F
    sudo iptables -X    
    
### Links

* [Forward a TCP port to another IP or port using NAT with Iptables](https://jensd.be/343/linux/forward-a-tcp-port-to-another-ip-or-port-using-nat-with-iptables)
* [Forward a TCP port to another IP or port using NAT with nftables](https://jensd.be/1086/linux/forward-a-tcp-port-to-another-ip-or-port-using-nat-with-nftables)

## Reverse Proxy

![](https://httpd.apache.org/docs/2.4/images/reverse-proxy-arch.png)

Quelle: Apache Web Server

- - -

Der Reverse Proxy ist ein Proxy, der Ressourcen für einen Client von einem oder mehreren Servern holt. Die Adressumsetzung wird in der entgegengesetzten Richtung vorgenommen, wodurch die wahre Adresse des Zielsystems dem Client verborgen bleibt. Während ein typischer Proxy dafür verwendet werden kann, mehreren Clients eines internen (privaten – in sich geschlossenen) Netzes den Zugriff auf ein externes Netz zu gewähren, funktioniert ein Reverse Proxy genau andersherum.

Für den Zugriff auf die obigen Microservices, ist ein Reverse Proxy besser geeignet. Ausserdem können wir für den Kunden eine kleine HTML Einstiegsseite zur Verfügung stellen.

Um einen Reverse Proxy bereitzustellen, braucht es lediglich eine weitere VM mit einem Apache Web Server. Die VM muss via VPN erreichbar sein. 

Dazu erstellen wir eine VMs, z.B. mit Namen apache-10 und weisen diese einem VPN zu.

Für die Bereitstellung (Deploy) verwenden wir folgendes [Cloud-init](apache.yaml) Script:

    #cloud-config
    packages:
      - apache2
    write_files:
     - content: |
        <html>
         <body>
          <h1>My Application</h1>
           <ul>
           <li><a href="/order">Order</a></li>
           <li><a href="/customer">Customer</a></li>
           <li><a href="/catalog">Catalog</a></li>
           </ul>
         </body>
        </html>
       path: /var/www/html/index.html
       permissions: '0644'  
     - content: |
        ProxyRequests Off
        <Proxy *>
              Order deny,allow
              Allow from all
        </Proxy>
        ProxyPass /order http://order-default         
        ProxyPassReverse /order http://order-default
        ProxyPass /customer http://customer-default
        ProxyPassReverse /customer http://customer-default 
        ProxyPass /catalog http://catalog-default         
        ProxyPassReverse /catalog http://catalog-default
       path: /etc/apache2/sites-enabled/001-reverseproxy.conf
       permissions: '0644'  
    runcmd:
     - sudo a2enmod proxy
     - sudo a2enmod proxy_html
     - sudo a2enmod proxy_http
     - sudo service apache2 restart 
     
## Terraform

Obige VMs können auch mittels Terraform angelegt werden. 

Dazu ist vorgängig die folgenden Umgebungsvariablen zu setzen:
* **TF_VAR_url** - URL des MAAS Servers
* **TF_VAR_key** - API Key des MAAS Servers. Siehe `ubuntu` -> `API keys`. **Tip**: separaten Key anlegen für Terraform.
* **TF_VAR_vpn** - VPN (AZ) welches verwendet werden soll

z.B.:

    TF_VAR_url=http://10.1.37.8:5240/MAAS
    TF_VAR_key=abcdef
    TF_VAR_vpn=10-1-37-8

Anschliesend das [Terraform CLI](https://www.terraform.io/downloads) installieren und die VMs wie folgt starten

    terraform init
    terraform apply
    
Soll ein anderer Name als `default` als Suffix verwendet werden, ist zuerst der Workspace zu wechseln und obige [Reverse Proxy](apache.yaml) Konfiguration anzupassen.

    terraform init
    terraform workspace new myname
    terraform apply
    
Werden die VMs nicht mehr benötigt, können sie auch wieder mittels Terraform gelöscht werden.

    terraform destroy    

### Links

* [Apache Reverse Proxy und LoadBalancer Guide](https://httpd.apache.org/docs/2.4/howto/reverse_proxy.html)
