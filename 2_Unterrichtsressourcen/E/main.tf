###
#   Webshop Beispiel 

module "apache" {
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-multipass"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-aws"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-azure"
  source     = "git::https://github.com/mc-b/terraform-lerncloud-maas"
  module     = "apache-${var.host_no}-${terraform.workspace}"
  userdata   = "apache.yaml"

  # Server Access Info
  url      = "${var.url}"
  key      = "${var.key}"
  vpn      = "${var.vpn}"  
}

module "order" {
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-multipass"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-aws"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-azure"
  source     = "git::https://github.com/mc-b/terraform-lerncloud-maas"
  module     = "order-${terraform.workspace}"
  userdata   = "order.yaml"

  # Server Access Info
  url      = "${var.url}"
  key      = "${var.key}"
}

module "customer" {
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-multipass"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-aws"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-azure"
  source     = "git::https://github.com/mc-b/terraform-lerncloud-maas"
  module     = "customer-${terraform.workspace}"
  userdata   = "customer.yaml"

  # Server Access Info
  url      = "${var.url}"
  key      = "${var.key}"
}

module "catalog" {
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-multipass"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-aws"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-azure"
  source     = "git::https://github.com/mc-b/terraform-lerncloud-maas"
  module     = "catalog-${terraform.workspace}"
  userdata   = "catalog.yaml"

  # Server Access Info
  url      = "${var.url}"
  key      = "${var.key}"
}


