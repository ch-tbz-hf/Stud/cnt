## Virtualisierung und Virtuelle Maschinen

[[_TOC_]]

### Infrastruktur

Als Infrastruktur wird die [lernMAAS](https://github.com/mc-b/lernmaas) Umgebung verwendet. 

Diese Implementiert eine Private Cloud und ist mittels VPNs (WireGuard) erreichbar.

**Das Vorgehen ist wie folgt:**

VPN [WireGuard](https://www.wireguard.com/install/) auf dem lokalen Notebook/PC installieren.

Vervollständigen der WireGuard Template Datei, z.B. `wg131-template.conf` mit Ihrer WireGuard IP-Adresse und dem privaten Key.

Dazu sind die Einträge <replace IP> und <replace Key> durch Ihre Werte, laut der Liste in den Unterlagen, zu ersetzen.

Die Konfigurationsdatei sieht in etwa so aus:

    [Interface]
    Address = <replace IP>/24
    PrivateKey = <replace Key>
    
    [Peer]
    PublicKey = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    Endpoint  = yyyyyyyyyyyyyyyyyy:518zz
    
    AllowedIPs = 192.168.xx.0/24

Starten Sie die WireGuard Software und fügen Sie die ergänzte WireGuard Vorlage als Tunnel hinzu:

![](../x_gitressourcen/wireguard-add.png)

Und aktivieren Sie den Tunnel:

![](../x_gitressourcen/wireguard-activate.png)

Nun befinden wir uns im VPN, wo sich auch die [lernMAAS](https://github.com/mc-b/lernmaas) Umgebung befindet und können diese mittels [http://w.x.y.z:5240](http://w.x.y.z:5240) anwählen.

### Beispielapplikation Webshop

![](https://github.com/mc-b/duk/raw/e85d53e7765f16833ccfc24672ae044c90cd26c1/data/jupyter/demo/images/Microservices-REST.png)

Quelle: Buch Microservices Rezepte
- - -

Für unseren Webshop wollen wir mindestens zwei VMs erstellen. Eine für die Applikation und eine für die Datenbank. Hinten an den Hostnamen wird '-' und eine fortlaufende Nummer angefügt.
Ersetzt  die Nummer, nach dem '-' durch Euren IP-Range. Und weisst eine Zone (VPN) zu.

Das Resultat sieht dann so aus:
* shop-02
* db-03

Beim Aufsetzen der [lernMAAS](https://github.com/mc-b/lernmaas) Umgebung haben wir auch gleichzeitig [VM Hosts](https://maas.io/docs/snap/2.9/ui/adding-a-vm-host) erstellt.

Ein [VM Hosts](https://maas.io/docs/snap/2.9/ui/adding-a-vm-host) ist eine Maschine, auf der virtuelle Maschinen (VMs) ausgeführt werden. Standardmässig verwendet MAAS dazu [KVM](https://www.linux-kvm.org/).

Über den Tab `KVM` im MAAS UI ist zuerst ein VM Host auszuwählen und dort die VMs zu [erstellen](https://maas.io/docs/snap/2.9/ui/creating-and-deleting-vms#heading--add-vm-from-ui).

**Links**

* [Give me an example of MAAS](https://maas.io/docs/maas-example-config)

### Maschinen Life Cycle

Jede von MAAS verwaltete Machine durchläuft einen Lebenszyklus (Stati):
* Registrierung (New), hardwarespezifische Elemente prüfen (Commissioning), Inventarisierung und Einrichtung von Firmware (Ready) 
* Bereitstellung (Deploy) 
um sie schliesslich zurück (Release) in den Ruhestand (Ready) zu entlassen.

Deployt beide VMs.

**Links**
* [About machines](https://maas.io/docs/snap/3.1/ui/about-machines#heading--about-the-machine-life-cycle)


### Secure Shell (SSH)

Secure Shell oder SSH bezeichnet ein kryptographisches Netzwerkprotokoll für den sicheren Betrieb von Netzwerkdiensten über ungesicherte Netzwerke. Häufig wird es verwendet, um lokal eine entfernte Kommandozeile verfügbar zu machen, d. h., auf einer lokalen Konsole werden die Ausgaben der entfernten Konsole ausgegeben, und die lokalen Tastatureingaben werden an den entfernten Rechner gesendet.

Auf dem [lernMAAS](https://github.com/mc-b/lernmaas) ist ein Default Public Key eingetragen. Dieser wird automatisch in jeder VM installiert (~/ssh/authorized_keys). Der dazugehörende Private Key kann vom Kursleiter verlangt werden.

Folgende Links erklären, wie man sich mit diesem Keys, via SSH vom Notebook mit der VM verbindet:
* [ssh in 6 Minuten](https://www.youtube.com/watch?v=v45p_kJV9i4)
* [Bitvise und ssh](https://www.youtube.com/watch?v=lJjEhWC0XYQ)

Verbindet auch mit den VMs via dem SSH Protokoll. Verwendet dabei das CLI `ssh` oder auf Windows [Bitvise](https://www.bitvise.com/).

Beispiel `ssh`:

    ssh -i id_public ubuntu@ip-vm

### Linux - Packages und Paket Manager (APT) 

Das [Advanced Packaging Tool (APT)](http://de.wikipedia.org/wiki/Advanced_Packaging_Tool) ist ein Paketverwaltungssystem, das im Bereich des Betriebssystems Debian GNU/Linux entstanden ist und [dpkg](http://de.wikipedia.org/wiki/Debian_Package_Manager) zur eigentlichen Paketverwaltung benutzt. Ziel ist es, eine einfache Möglichkeit zur Suche, Installation und Aktualisierung von Programmpaketen zur Verfügung zu stellen. APT besteht aus einer Programmbibliothek und mehreren diese Bibliothek nutzenden Kommandozeilen-Programmen, von denen **apt-get** und apt-cache zentral sind. Seit Debian 3.1 wird die Benutzung von aptitude als konsolenbasierendes Paketverwaltungssystem empfohlen.

In der Datei **/etc/apt/sources.list** stehen die sogenannten Repositories, also Quellen für Pakete. Dies können entweder CDs oder DVDs, Verzeichnisse auf der Festplatte oder, öfter, Verzeichnisse auf HTTP- oder FTP-Servern sein. Befindet sich das gesuchte Paket auf einem Server (oder einem lokalen Datenträger), so wird dieses automatisch heruntergeladen und installiert.

Die Pakete liegen im Debian-Paketformat (.deb) vor, in dem auch die jeweiligen Abhängigkeiten der Programmpakete untereinander abgelegt sind. So werden automatisch für ein Programm auch eventuell erforderliche Programmbibliotheken mit heruntergeladen und installiert.

**Nützliche Befehle** 
   
- `sudo apt-get update`- Paketindex (Software-Repositories) aktualisieren
- `sudo apt-get -y install apache2` - Webserver Apache installieren
- `sudo apt-get -y upgrade`- bestehende Software aktualisieren
- `sudo apt-get -y autoremove` - Aufräumen, nicht mehr benötigte Software entfernen                        
- `sudo apt-cache search [keyword]` - Suchen nach einem bestimmen Programmpaket.
- `sudo dpkg -i [Programmpaket]` - Installieren eines vorher downloadeten Programmpaketes

**Testen**

Paketindex (Software-Repositories) aktualisieren (empfohlen)

    sudo apt-get update
    
Apache Webserver Installieren

    sudo apt-get install apache2 
    
Testen ob Apache Webserver läuft

    curl localhost
    
**Tipp**

Pakete über das Internet zu laden, kann relativ grossen Netzwerkverkehr verursachen. Deshalb sollten Paketquellen aus der Schweiz verwendet werden, z.B. `https://mirror.init7.net/ubuntu/`.

Die Paketquellen können in der MAAS Oberfläche unter `Settings` -> `Packages repos` angepasst werden. Dabei ist der erste Eintrag mit `http://archive.ubuntu.com/ubuntu` zu ersetzen. Bei neuen Deployen der VMs werden dann diese verwendet.

**Links**

*   [Debian-Paketverwaltung verwenden](https://learning.lpi.org/de/learning-materials/101-500/102/102.4/)
*   [Paketmanagement](http://debiananwenderhandbuch.de/paketmanagement.html)
*   [Systemsicherheit](http://debiananwenderhandbuch.de/sicherheit.html)
*   [25 Useful Basic Commands of APT-GET and APT-CACHE for Package Management](http://www.tecmint.com/useful-basic-commands-of-apt-get-and-apt-cache-for-package-management/)

