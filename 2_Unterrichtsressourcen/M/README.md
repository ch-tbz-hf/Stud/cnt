## Kubevirt

Die KubeVirt-Technologie erfüllt die Anforderungen von Entwicklungsteams, die Kubernetes eingeführt haben oder übernehmen möchten, aber über vorhandene VM-basierte Workloads verfügen , die nicht einfach in Container umgewandelt werden können. 

Genauer gesagt bietet die Technologie eine einheitliche Entwicklungsplattform, auf der Entwickler Anwendungen erstellen, ändern und bereitstellen können, die sich sowohl in Containern als auch auf virtuellen Maschinen in einer gemeinsamen, gemeinsam genutzten Umgebung befinden.

Die Vorteile sind breit gefächert und bedeutend. Teams, die sich auf vorhandene VM-basierte Workloads verlassen, können Anwendungen schnell in Container umwandeln. Mit virtualisierten Workloads, die direkt in Entwicklungsworkflows platziert werden, können Teams sie im Laufe der Zeit zerlegen und gleichzeitig verbleibende virtualisierte Komponenten wie gewünscht nutzen.

Einsatzbereiche:
* Nutzen Sie KubeVirt und Kubernetes, um virtuelle Maschinen für unpraktisch zu containerisierende Anwendungen zu verwalten.
* Kombinieren Sie vorhandene virtualisierte Workloads mit neuen Container-Workloads auf einer Plattform.
* Unterstützen Sie die Entwicklung neuer Microservice-Anwendungen in Containern, die mit bestehenden virtualisierten Anwendungen interagieren.

### Installation

    runcmd:
        - sudo apt-get update
        - sudo snap install microk8s --classic --channel=1.22
        - sudo snap install kubectl --classic --channel=1.22
        - sudo microk8s enable dns rbac storage
        # Zugriff fuer User ubuntu einrichten - funktioniert erst wenn microk8s laeuft
        - sudo usermod -a -G microk8s ubuntu
        - sudo mkdir -p /home/ubuntu/.kube
        - sudo microk8s config | sudo tee  /home/ubuntu/.kube/config
        - sudo chown -f -R ubuntu:ubuntu /home/ubuntu/.kube
        # KVM neested virtualisation
        - sudo apt-get update && sudo apt-get install qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils -y
        - sudo adduser ubuntu libvirt
        - sudo adduser ubuntu kvm
        # kubevirt
        - export VERSION=$(curl -s https://api.github.com/repos/kubevirt/kubevirt/releases | grep tag_name | grep -v -- '-rc' | sort -r | head -1 | awk -F': ' '{print $2}' | sed 's/,//' | xargs)
        - echo $VERSION
        - export ARCH=$(uname -s | tr A-Z a-z)-$(uname -m | sed 's/x86_64/amd64/') || windows-amd64.exe
        - echo ${ARCH}
        # virtctl
        - curl -L -o virtctl https://github.com/kubevirt/kubevirt/releases/download/${VERSION}/virtctl-${VERSION}-${ARCH}
        - chmod +x virtctl
        - sudo install virtctl /usr/local/bin    
        - sudo microk8s kubectl create -f https://github.com/kubevirt/kubevirt/releases/download/${VERSION}/kubevirt-operator.yaml
        - sudo microk8s kubectl create configmap kubevirt-config -n kubevirt --from-literal debug.useEmulation=true
        - sudo microk8s kubectl create -f https://github.com/kubevirt/kubevirt/releases/download/${VERSION}/kubevirt-cr.yaml

Leider crasht der virt Pod, darum nicht funktionsfähig.
    
    
**Links**

* [KVM Install](https://phoenixnap.com/kb/ubuntu-install-kvm)
* [Kubevirt Install](https://kubevirt.io/quickstart_kind/)


    
    
