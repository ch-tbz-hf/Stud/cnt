terraform {
  required_providers {
    github = {
      source  = "integrations/github"
      version = "~> 4.0"
    }
  }
}

# Configure the GitHub Provider
provider "github" {
    token       = var.token # or `GITHUB_TOKEN`
}

######
#
#  Repository

resource "github_repository" "cnt_work" {
  name              = var.project_name
  description       = "Arbeitsbereich"

  visibility        = "public"
  has_projects      = true
  has_issues        = true 
  auto_init         = true
}

 


