#######
#
#   GitHub Projekte zum Planen der LBs

resource "github_repository_project" "lb1" {
  name = "LB1: MAAS – Cloud-init"
  body = " MAAS – Metal as a Service und Cloud (Cloud-init)"
  repository = "${github_repository.cnt_work.name}"
}

resource "github_repository_project" "lb2" {
  name = "LB2: MAAS – Cloud-init"
  body = " MAAS – Metal as a Service und Cloud (Private Cloud)"
  repository = "${github_repository.cnt_work.name}"
}

resource "github_repository_project" "lb3" {
  name = "LB3: Container"
  body = " Container und Kubernetes, DevOps (Container)"
  repository = "${github_repository.cnt_work.name}"
}

resource "github_repository_project" "lb4" {
  name = "LB4: Kubernetes"
  body = " Container und Kubernetes, DevOps (Kubernetes)"
  repository = "${github_repository.cnt_work.name}"
}
