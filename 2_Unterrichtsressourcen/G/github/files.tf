#######
#
#   Verzeichnisstruktur analog Kompetenzmatrix 

resource "github_repository_file" "readme" {
    repository          = github_repository.cnt_work.name
    file                = "README.md"
    content             = file("${path.module}/README.md")
    commit_message      = var.initial_commit_message
    overwrite_on_create = true
}

resource "github_repository_file" "a_readme" {
    repository          = github_repository.cnt_work.name
    file                = "A/README.md"
    content             = file("${path.module}/A/README.md")
    commit_message      = var.initial_commit_message
}

resource "github_repository_file" "b_readme" {
    repository          = "${github_repository.cnt_work.name}"
    file                = "B/README.md"
    content             = file("${path.module}/B/README.md")
    commit_message      = var.initial_commit_message
}

resource "github_repository_file" "c_readme" {
    repository          = "${github_repository.cnt_work.name}"
    file                = "C/README.md"
    content             = file("${path.module}/C/README.md")
    commit_message      = var.initial_commit_message
}

resource "github_repository_file" "d_readme" {
    repository          = github_repository.cnt_work.name
    file                = "D/README.md"
    content             = file("${path.module}/D/README.md")
    commit_message      = var.initial_commit_message
}

resource "github_repository_file" "e_readme" {
    repository          = "${github_repository.cnt_work.name}"
    file                = "E/README.md"
    content             = file("${path.module}/E/README.md")
    commit_message      = var.initial_commit_message
}

resource "github_repository_file" "f_readme" {
    repository          = "${github_repository.cnt_work.name}"
    file                = "F/README.md"
    content             = file("${path.module}/F/README.md")
    commit_message      = var.initial_commit_message
}