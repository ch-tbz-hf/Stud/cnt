
# Allgemeine Variablen

# der Access Token wird in einer separaten Datei token.tf gesetzt.
#variable "token" {
#  description = "Access Token"
#  default = "..."
#}

variable "project_name" {
  description = "Projekt Name"
  default = "cnt-work"
}

variable "initial_commit_message" {
  description = "Bemerkungen"
  default = "Initial commit"
}
