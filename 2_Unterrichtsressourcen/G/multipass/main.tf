terraform {
  required_providers {
    null = {
      source = "hashicorp/null"
    }
  }
}

resource "null_resource" "webshop" {
  triggers = {
    name = "webshop"
  }
  provisioner "local-exec" {
    command = "multipass launch --name webshop --cloud-init ${path.module}/../scripts/webshop.yaml"
  }
  provisioner "local-exec" {
    when    = destroy
    command = "multipass delete ${self.triggers.name} --purge"
  }
}

resource "null_resource" "order" {
  triggers = {
    name = "order"
  }
  provisioner "local-exec" {
    command = "multipass launch --name order --cloud-init ${path.module}/../scripts/order.yaml"
  }
  provisioner "local-exec" {
    when    = destroy
    command = "multipass delete ${self.triggers.name} --purge"
  }
}

resource "null_resource" "customer" {
  triggers = {
    name = "customer"
  }
  provisioner "local-exec" {
    command = "multipass launch --name customer --cloud-init ${path.module}/../scripts/customer.yaml"
  }
  provisioner "local-exec" {
    when    = destroy
    command = "multipass delete ${self.triggers.name} --purge"
  }
}

resource "null_resource" "catalog" {
  triggers = {
    name = "catalog"
  }
  provisioner "local-exec" {
    command = "multipass launch --name catalog --cloud-init ${path.module}/../scripts/catalog.yaml"
  }
  provisioner "local-exec" {
    when    = destroy
    command = "multipass delete ${self.triggers.name} --purge"
  }
}
