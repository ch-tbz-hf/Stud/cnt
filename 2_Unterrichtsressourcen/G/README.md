## Terraform

[[_TOC_]]

### Einleitung

[![](https://embed-fastly.wistia.com/deliveries/41c56d0e44141eb3654ae77f4ca5fb41.jpg)](https://learn.hashicorp.com/tutorials/terraform/infrastructure-as-code?in=terraform%2Faws-get-started&amp;wvideo=mo76ckwvz4)

Quelle: hashicorp
- - -

Terraform ist ein Open-Source- Infrastruktur als Code- Software-Tool, das von HashiCorp entwickelt wurde . Benutzer definieren und stellen die Rechenzentrumsinfrastruktur mithilfe einer deklarativen Konfigurationssprache namens HashiCorp Configuration Language (HCL) oder optional JSON bereit.

So stellen Sie die Infrastruktur mit Terraform bereit:

* **Scope**  - Identifizieren Sie die Infrastruktur für Ihr Projekt.
* **Author**  – Schreiben Sie die Konfiguration für Ihre Infrastruktur.
* **Initialize** `terraform init` – Installieren Sie die Plugins, die Terraform zum Verwalten der Infrastruktur benötigt.
* **Plan** `terraform plan` – Zeigen Sie eine Vorschau der Änderungen an, die Terraform an Ihre Konfiguration anpasst.
* **Apply** `terraform apply` – Nehmen Sie die geplanten Änderungen vor.
* **Destroy** `terraform destroy` – Löschen Sie die Infrastruktur, wenn Sie sie nicht mehr benötigen. 

**Tipp**: jede Cloud Plattform erlaubt die eben durchgeführte Aktion, z.B. das erstellen einer VM, als Template zu speichern. In diesem Template finden Sie die Werte welche nachher in die Terraform Konfiguration übertragen werden können, z.B. der Name einer VM-Vorlage.

Für die nachfolgenden Beispiele sind die CLI für Azure, AWS und das Terraform CLI zu installieren.

* [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/)
* [AWS CLI](https://aws.amazon.com/de/cli/)
* [Terraform Installation](https://learn.hashicorp.com/tutorials/terraform/install-cli?in=terraform/aws-get-started)

Der Verzeichnisaufbau ist wie folgt:
* **aws** - Verzeichnis mit den AWS spezifischen Deklarationen
* **azure** - Verzeichnis mit den Azure spezifischen Deklarationen
* **maas** - Verzeichnis mit den MAAS spezifischen Deklarationen
* **scripts** - Cloud-init Script, werden von den Cloud Deklarationen verwendet im immer die gleiche SW auf den VMs zu installieren.
* **github** - Beispiel für ein Github Repository mit Projekte zum Planen.

Ausserdem existiert eine `main.tf` Datei in diesem Verzeichnis. Diese `main.tf` erlaubt es alle nachfolgenden Beispiele gesamthaft Auszuführen. 

### Azure Cloud

Implementiert den [Webshop](../A#beispielapplikation-webshop) in der Azure Cloud mittels Terraform.

Starten mittels

    az login
    git clone https://gitlab.com/ch-tbz-hf/Stud/cnt.git          
    cd cnt/2_Unterrichtsressourcen/G/azure
    terraform init
    terraform apply -auto-approve

Nach dem erstellen der Ressourcen, VM `webshop` anwählen und IP-Adresse im Browser eingeben.

**Links**

* [Virtuelle Maschinen erstellen](https://github.com/hashicorp/terraform-provider-azurerm/tree/main/examples/virtual-machines)

### AWS Cloud

Implementiert den [Webshop](../A#beispielapplikation-webshop) in der AWS Cloud mittels Terraform.

Starten mittels

    aws configure
        AWS Access Key ID [****************....]:
        AWS Secret Access Key [****************....]:
        Default region name [us-west-2]: us-east-1
        Default output format [None]:  
    git clone https://gitlab.com/ch-tbz-hf/Stud/cnt.git          
    cd cnt/2_Unterrichtsressourcen/G/aws
    terraform init
    terraform apply -auto-approve

Nach dem erstellen der Ressourcen, wird die URL der VM mit dem Reverse Proxy und dem Menu, zum Anwählen, der anderen VMs angezeigt.

Vorher muss aber die Datei `/etc/host` in der Reverse Proxy VM so angepasst werden. Um dort die Namen und IP-Adressen der VMs `order`, `customer` und `catalog` zu erfassen.
Sollte auch mit einer Private Route 53 Zone möglich sein, die Terraform Einträge sind jedoch nicht klar.

**Links**

* [Basic Two-Tier AWS Architecture](https://github.com/hashicorp/terraform-provider-aws/tree/main/examples/two-tier)
* [Umfangreicheres Beispiel mit VPC, NAT etc.](https://github.com/hashicorp/learn-terraform-modules-create)

### MAAS

Implementiert den [Webshop](../A#beispielapplikation-webshop) in der MAAS Umgebung mittels Terraform.

**Vorgehen**

Anlegen von 4 Maschinen
* 3 VMs mit Defaultwerten ohne AZ
* 1 VM mit Namen `apache-09` und zugewiesener AZ (VPN)

Anpassen der Zugriffsinformationen auf die MAAS Umgebung in `maas/main.tf`, Variablen `api_key` und `api_url`.

    git clone https://gitlab.com/ch-tbz-hf/Stud/cnt.git          
    cd cnt/2_Unterrichtsressourcen/G/maas
    terraform init
    terraform apply -auto-approve    

**Hintergrund**

Für die MAAS Umgebungen existieren zwei Provider für Terraform
* [suchpuppet](https://github.com/suchpuppet/terraform-provider-maas) - Reicht zum Deployen und Releasen von vorhanden Maschinen.
* [ionutbalutoiu](https://github.com/ionutbalutoiu/terraform-provider-maas) - Umfangreicher aber schlecht Dokumentiert und nicht in Terraform Registry.

Das Beispiel verwendet den ersten Provider, weil dieser via Terraform Registry angesprochen werden kann.

### Github

![](https://wahlnetwork.com/wp-content/uploads/2020/08/image-5.png)

Quelle: Using Terraform to Manage Git Repositories
- - -

GitHub ist ein netzbasierter Dienst zur Versionsverwaltung für Software-Entwicklungsprojekte. Namensgebend war das Versionsverwaltungssystem Git.

Um die Dokumentation und Planung des eigenen Projektes zu vereinfachen, existiert eine Terraform Konfiguration. Mit dieser kann der Grundlayout eines Arbeitsrepositories erstellt werden.

Dazu ist zuerst ein [personal access token](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token) in Github zu erstellen um diesen nachher in der `variables.tf` einzutragen.

    git clone https://gitlab.com/ch-tbz-hf/Stud/cnt.git          
    cd cnt/2_Unterrichtsressourcen/G/github
    # personal access token in variables.tf eintragen
    terraform init
    terraform apply -auto-approve    

**Links**

* [Creating a personal access token](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token)
* [Using Terraform to Manage Git Repositories](https://wahlnetwork.com/2020/09/01/using-terraform-to-manage-git-repositories/)
* [How to manage your GitHub Organization with Terraform](https://www.mineiros.io/blog/how-to-manage-your-github-organization-with-terraform)
* [Terraform tips & tricks: loops, if-statements, and gotchas](https://blog.gruntwork.io/terraform-tips-tricks-loops-if-statements-and-gotchas-f739bbae55f9)
* [Why we use Terraform and not Chef, Puppet, Ansible, SaltStack, or CloudFormation](https://blog.gruntwork.io/why-we-use-terraform-and-not-chef-puppet-ansible-saltstack-or-cloudformation-7989dad2865c) 
* [Terraform: Up & Running, 3rd edition](https://blog.gruntwork.io/terraform-up-running-3rd-edition-early-release-is-now-available-4efd0eb2ce0a)