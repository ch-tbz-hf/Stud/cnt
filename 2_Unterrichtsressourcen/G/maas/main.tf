###
#   Provider "maas" verwenden, sonst Fehler!

terraform {
  required_providers {
    maas = {
      source = "mc-b/lernmaas"
      version = "~>1.0.1"
    }
  }
}

provider "maas" {
  # Configuration options
  api_version = "2.0"
  api_key = "nC83nVyLDWKF8zxvPq:VnYukvR9Yh3w9jRDUe:FdU7sFWJu8DHjAeRumRNmZfasNBDqgXa"
  api_url = "http://10.6.37.8:5240/MAAS"
}

###
#   KVM Hosts

data "maas_vm_hosts" "vm-hosts" {
  id  = "rack-01"
}

###
#   VMs 

resource "maas_vm_instance" "apache-09" {
    hostname = "apache-09"
    kvm_no = data.maas_vm_hosts.vm-hosts.recommended  
    zone =  "10-6-37-0"  
    user_data = data.template_file.webshop.rendered
}

resource "maas_vm_instance" "order" {
    hostname = "order"
    kvm_no = data.maas_vm_hosts.vm-hosts.recommended  
    user_data = data.template_file.order.rendered
}

resource "maas_vm_instance" "customer" {
    hostname = "customer"
    kvm_no = data.maas_vm_hosts.vm-hosts.recommended  
    user_data = data.template_file.customer.rendered
}

resource "maas_vm_instance" "catalog" {
    hostname = "catalog"
    kvm_no = data.maas_vm_hosts.vm-hosts.recommended  
    user_data = data.template_file.catalog.rendered
}

