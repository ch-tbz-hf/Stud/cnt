#######
#
#   Hauptmodul - vereint alle anderen Module 

# Git Repository

module "github" {
  source    = "./github"
}

# Webshop in Azure

module "azure" {
  source    = "./azure"
}


# Webshop in AWS

module "aws" {
  source    = "./aws"
}

# Webshop in MAAS.io

module "maas" {
  source    = "./maas"
}

# Webshop in lokal via Multipass

module "multipass" {
  source    = "./multipass"
}

