###
#   Outputs wie IP-Adresse und DNS Name
#  

output "ip_vm_docker" {
  value = module.docker.ip_vm
  description = "The IP address of the server instance."
}

output "fqdn_vm_docker" {
  value = module.docker.fqdn_vm
  description = "The FQDN of the server instance."
}

# Einfuehrungsseite(n)

output "README" {
  value = templatefile( "INTRO.md", { ip = module.docker.ip_vm, fqdn = module.docker.fqdn_vm, ADDR = module.docker.ip_vm } )
} 
