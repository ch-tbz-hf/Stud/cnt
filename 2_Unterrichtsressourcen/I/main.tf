###
#   Testumgebung Docker 

module "docker" {
  source     = "git::https://github.com/mc-b/terraform-lerncloud-multipass.git"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-aws.git"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-azure.git"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-maas.git"
  module     = "docker-${var.host_no + 1}-${terraform.workspace}"
  userdata   = "../H/cloud-init-docker.yaml"
  
  # Server Access Info
  url      = "${var.url}"
  key      = "${var.key}"
  vpn      = "${var.vpn}"   
}
