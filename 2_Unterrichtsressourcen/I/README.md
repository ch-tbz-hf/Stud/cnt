## Container Laufzeitumgebungen (Runtimes)

[[_TOC_]]


### Container

Container ändern die Art und Weise, wie wir Software entwickeln, verteilen und laufen lassen, grundlegend.

Entwickler können Software lokal bauen, die woanders genauso laufen wird – sei es ein Rack in der IT-Abteilung, der Laptop eines Anwenders oder ein Cluster in der Cloud.

Administratoren können sich auf die Netzwerke, Ressourcen und die Uptime konzentrieren und müssen weniger Zeit mit dem Konfigurieren von Umgebungen und dem Kampf mit Systemabhängigkeiten verbringen.

**Merkmale** 

* Container teilen sich Ressourcen mit dem Host-Betriebssystem
* Container können im Bruchteil einer Sekunde gestartet und gestoppt werden
* Anwendungen, die in Containern laufen, verursachen wenig bis gar keinen Overhead
* Container sind portierbar --> Fertig mit "Aber bei mir auf dem Rechner lief es doch!"
* Container sind leichtgewichtig, d.h. es können dutzende parallel betrieben werden.
* Container sind "Cloud-ready"!

### Docker

[![](https://img.youtube.com/vi/YFl2mCHdv24/0.jpg)](https://www.youtube.com/watch?v=YFl2mCHdv24)

Docker YouTube Einführung
- - -

Docker nahm die bestehende Linux-Containertechnologie auf und verpackte und erweiterte sie in vielerlei Hinsicht – vor allem durch portable Images und eine benutzerfreundliche Schnittstelle –, um eine vollständige Lösung für das Erstellen und Verteilen von Containern zu schaffen.

Die Docker-Plattform besteht vereinfacht gesagt aus zwei getrennten Komponenten: der Docker Engine, die für das Erstellen und Ausführen von Containern verantwortlich ist, sowie dem Docker Hub, einem Cloud Service, um Container-Images zu verteilen.

#### Architektur

Nachfolgend sind die wichtigsten Komponenten von Docker aufgelistet:

**Docker Daemon** <br>
* Erstellen, Ausführen und Überwachen der Container
* Bauen und Speichern von Images
* Ab Version 1.11 wurde der Docker Daemon in zwei Prozesse `runc` zum Starten von Container und `containerd` zum Betreiben von Container unterteilt.

**Docker Client** <br> 
* Docker wird über die Kommandozeile (CLI) mittels des Docker Clients bedient
* Kommuniziert per HTTP(S) REST mit dem Docker Daemon

Da die gesamte Kommunikation über HTTP(S) abläuft, ist es einfach, sich mit entfernten Docker Daemons zu verbinden und Bindings an Programmiersprachen zu entwickeln.

**Images** <br> 
* Images sind gebuildete Umgebungen welche als Container gestartet werden können
* Images sind nicht veränderbar, sondern können nur neu gebuildet werden.
* Images bestehen aus Namen und Version (TAG), z.B. *ubuntu:16.04.* 
    * Wird keine Version angegeben wird automatisch :latest angefügt.

**Container** <br> 
* Container sind die ausgeführten Images
* Ein Image kann beliebig oft als Container ausgeführt werden
* Container bzw. deren Inhalte können verändert werden, dazu werden sogenannte *Union File Systems* verwendet, welche nur die Änderungen zum original Image speichern.

**(Docker) Container Registry** <br> 
* In Container Registries werden Images abgelegt und verteilt

Die Standard-Registry ist der [Docker Hub](http://hub.docker.com), auf dem tausende öffentlich verfügbarer Images zur Verfügung stehen, aber auch "offizielle" Images.

Viele Organisationen und Firmen nutzen eigene Registries, um kommerzielle oder "private" Images zu hosten, aber auch um den Overhead zu vermeiden, der mit dem Herunterladen von Images über das Internet einhergeht. 

### ContainerD

containerd ist als Daemon für Linux und Windows verfügbar. Es verwaltet den gesamten Container-Lebenszyklus seines Host-Systems, von der Image-Übertragung und -Speicherung über die Container-Ausführung und -Überwachung bis hin zum Low-Level-Storage und Netzwerk. 

* Es ist aus Docker entstanden, ist vereinfacht dessen Container Runtime.
* **Adaptors**: IBM Cloud, Google, CloudFoundry, Docker, etc.. 

### CRI-O

CRI-O: das 2016 gestartete und 2017 vorgestellte CRI-O baut auf dem Kubernetes Container Runtime Interface (CRI) auf.  

CRI-O erlaubt das direkte Verwenden von OCI-kompatiblen (Open Container Initiative) Containern in Kubernetes, ohne zusätzlichen Code oder weiteres Tooling. 

* **Contributors** sind RedHat, Intel, SUSE, IBM. 


### Hands-on

Für die Hands-on kann die VM mit dem Cloud-init Script aus [Container Technologie](../H/web-app-container.md) verwendet werden.

* [Einfache Web-App in einen Container  verfrachten](https://gitlab.com/ser-cal/Container-CAL-webapp_v1)
* Verwendet statt der Docker Image Registry [GitLab](https://docs.gitlab.com/ee/user/packages/container_registry/), [GitHub](https://docs.github.com/en/packages/working-with-a-github-packages-registry/working-with-the-container-registry) oder eine Cloud basierte Registry.
* [Weitere Beispiele aus dem Modul 300](https://github.com/mc-b/M300/tree/master/docker)

### Hinweise

Um die [DockerHub download rate limits](https://microk8s.io/docs/dockerhub-limits) zu Umgehen ist es ratsam die Container Images nicht auf `hub.docker.com` abzulegen. Als Alternative eignet sich `https://registry.gitlab.com/`. Dazu ist zuerst ein Account auf gitlab anzulegen, dann ein Repository zu erstellen.

Um dann ein Container Image in diesem Repository abzulegen ist wie folgt vorzugehen:

    docker login registry.gitlab.com
    docker build -t httpd registry.gitlab.com/<user>/<repository>
    docker push registry.gitlab.com/<user>/<repository>
    
Oder um ein Container Image von `hub.docker.com` auf `registry.gitlab.com/<user>/<repository>` zu verschieben

    docker pull httpd
    docker tag httpd registry.gitlab.com/<user>/<repository>/httpd
    docker push httpd registry.gitlab.com/<user>/<repository>/httpd

* [DockerHub download rate limits](https://microk8s.io/docs/dockerhub-limits)
