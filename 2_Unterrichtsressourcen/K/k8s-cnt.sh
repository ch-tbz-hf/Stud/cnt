#!/bin/bash
#
#   microk8s Erweiterungen
# 

# Ingress (Reverse Proxy)
sudo microk8s enable ingress

# Dashboard
microk8s kubectl apply -f https://raw.githubusercontent.com/mc-b/lerncloud/master/addons/dashboard.yaml 

# Jupyter Notebooks 
su - ubuntu -c "git clone https://gitlab.com/ch-tbz-hf/Stud/cnt.git"
su - ubuntu -c "cp -rp cnt/2_Unterrichtsressourcen/K/jupyter /data/"
sudo microk8s kubectl apply -f https://raw.githubusercontent.com/mc-b/duk/master/jupyter/jupyter-base-microk8s.yaml

# IP fuer Notebooks
export ADDR=$(ip -f inet addr show wg0 | grep -Po 'inet \K[\d.]+')

if [ "${ADDR}" != "" ]
then
    echo ${ADDR} >/data/jupyter/server-ip
else
    echo $(hostname -I | cut -d ' ' -f 1) >/data/jupyter/server-ip
fi 

# AWS Public Hostname
export ADDR=$(curl --max-time 2 http://169.254.169.254/latest/meta-data/public-hostname)
[ "${ADDR}" != "" ] && {  echo ${ADDR} >/data/jupyter/server-ip; }

chown ubuntu:ubuntu /data/jupyter/server-ip