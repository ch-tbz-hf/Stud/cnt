## Kubernetes

[[_TOC_]]

[![](https://img.youtube.com/vi/PH-2FfFD2PU/0.jpg)](https://www.youtube.com/watch?v=PH-2FfFD2PU)

Kubernetes in 5 Minuten auf YouTube

---

Kubernetes (auch als „K8s“ oder einfach „K8“ bezeichnet - *sprich immer: 'Kuhbernetties'*) ist ein Open-Source-System zur Automatisierung der Bereitstellung, Skalierung und Verwaltung von Container-Anwendungen, das ursprünglich von Google entworfen und an die Cloud Native Computing Foundation gespendet wurde. Es zielt darauf ab, eine „Plattform für das automatisierte Bespielen, Skalieren und Warten von Anwendungscontainern auf verteilten Hosts“ zu liefern. Es unterstützt eine Reihe von Container-Tools, einschließlich Docker.

Die Orchestrierung mittels Kubernetes wird von führenden Cloud-Plattformen wie Microsofts Azure, IBMs Bluemix, Red Hats OpenShift und Amazons AWS unterstützt.

### Merkmale

* Immutable (Unveränderlich) statt Mutable.
* Deklarative statt Imperative (Ausführen von Anweisungen) Konfiguration.
* Selbstheilende Systeme - Neustart bei Absturz.
* Entkoppelte APIs – LoadBalancer / Ingress (Reverse Proxy).
* Skalieren der Services durch Änderung der Deklaration.
* Anwendungsorientiertes statt Technik (z.B. Route 53 bis AWS) Denken.
* Abstraktion der Infrastruktur statt in Rechnern Denken.

### Wichtige Begriffe

**Lastverteilung (Load Balancing)**

Mittels Lastverteilung (englisch Load Balancing) werden in der Informatik umfangreiche Berechnungen oder große Mengen von Anfragen auf mehrere parallel arbeitende Systeme verteilt. 

Insbesondere bei Webservern ist eine Lastverteilung wichtig, da ein einzelner Host nur eine begrenzte Menge an HTTP-Anfragen auf einmal beantworten kann. 

Für unsere Zwecke kann Lastverteilung als der Prozess des Verteilens von Anfragen auf verschiedene Container betrachtet werden.

**Cluster**

Ein [Rechnerverbund oder Computercluster](https://de.wikipedia.org/wiki/Rechnerverbund), meist einfach Cluster genannt (vom Englischen für „Rechner-Schwarm“, „-Gruppe“ oder „-Haufen“), bezeichnet eine Anzahl von vernetzten Computern. 

Der Begriff wird zusammenfassend für zwei unterschiedliche Aufgaben verwendet: 
* die Erhöhung der Rechenkapazität (HPC-Cluster) 
* die Erhöhung der Verfügbarkeit (HA-Cluster, engl. high available - hochverfügbar). 

Die in einem Cluster befindlichen Computer (auch Knoten, vom englischen nodes oder Server) werden auch oft als Serverfarm bezeichnet.

#### Hands-On

* [Kubernetes (Ubuntu mit microk8s) Installieren](kubernetes.md) 
* Erstellt mehrere VMs mittels Cloud-init und Verbindet diese zu einem [Kubernetes Cluster](https://microk8s.io/docs/clustering).
* Ergänzt Euren Cluster mit dem [Dashboard](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/). **Tip** [Dashboard ohne Login](https://github.com/mc-b/duk/tree/master/addons) verwenden.
* [Windows-Subsystem für Linux mit Kubernetes](wsl/) (optional, unsupported)
* [MultiCloud Kubernetes](multicloud/)
* [K3s](distributions/k3s/) (optional, unsupported)
* [OpenShift](https://github.com/openshift/okd) (optional)
* [Rancher](https://rancher.com/quick-start) by [SUSE](https://www.suse.com/de-de/products/suse-rancher/) (optional)
* [Kind](distributions/kind/) (optional, unsupported)

#### Hands-On: Kubernetes in der Cloud (optional)

* [Amazon Elastic Kubernetes Service (EKS)](https://aws.amazon.com/de/eks/) und [Terraform](cloud/aws/)
* [Azure Kubernetes Service (AKS)](https://azure.microsoft.com/de-de/services/kubernetes-service/)
* [Google Kubernetes Engine](https://cloud.google.com/kubernetes-engine)

### Ressourcen (Objects)

Kubernetes stellt eine Vielzahl von Ressourcen (Objecten) zur Verfügung. Die wichtigsten sind:

* **Pod** - Ein [Pod](https://kubernetes.io/docs/concepts/workloads/pods/pod/) repräsentiert eine Gruppe von Anwendungs-Containern und Volumes,
die in der gleichen Ausführungsumgebung (gleiche IP, Node) laufen.
* **Service**: Ein [Service](https://kubernetes.io/docs/concepts/services-networking/service/) steuert den Zugriff auf einen Pod (IP-Adresse, Port). Während Pods (bzw. Images) ersetzt werden können (z.B. durch Update auf neue Version) bleibt ein Service stabil.
* **Ingress**: Ähnlich einem Reverse Proxy ermöglicht ein [Ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/) den Zugriff auf einen Service über einen URL.
* **ReplicaSet**: [ReplicaSets](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/) bestimmen wieviele Exemplare eines Pods laufen und stellen sicher, dass die angeforderte Menge auch verfügbar ist. 
* **Deployment**: [Deployments](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/) erweitern ReplicaSets um deklarative Updates (z.B. von Version 1.0 auf 1.1) von Container Images.

Weitere siehe [Kubernetes Concepts](https://kubernetes.io/docs/concepts/).

Unterhaltsame Broschüre mit dem Titel ["Phippy Goes to the Zoo – A Kubernetes Story"](https://azure.microsoft.com/en-us/resources/phippy-goes-to-the-zoo/en-us/), zeigt in der Form eines Kinderbuches wesentliche Objekte von Kubernetes auf.

#### Hands-On

Die Hands-on basieren auf [Jupyter Notebooks](https://jupyter-tutorial.readthedocs.io/de/latest/index.html). Diese können Live in der Kubernetes VM ausgeführt werden.
Wie steht im Hands-on [Kubernetes](kubernetes.md).

Werden die Befehle in der Kommandozeile der VM ausgeführt, ist jeweils das `|` oder `%%bash` wegzulassen.

**Container und Service Discovery**

* [Pods und Services (kubectl-Variante)](jupyter/09-1-kubectl.ipynb)
* [Pods und Services (YAML-Variante)](jupyter/09-2-YAML.ipynb)
* [Ingress (Reverse Proxy)](jupyter/09-2-Ingress.ipynb)

**Lastverteilung und Selbstheilung**

* [ReplicaSet](jupyter/09-3-ReplicaSet.ipynb)

**Persistenz**

Vor dem Ausführen dieser Hands-on ist, wenn nicht bereits erfolgt via [kubernetes.md](kubernetes.md), ein Verzeichnis `/data` anzulegen und ein Persistent Volume anzulegen.

    sudo mkdir /data
    sudo chmod 777 /data
    kubectl apply -f https://raw.githubusercontent.com/mc-b/lernkube/master/data/DataVolume.yaml

Worker Nodes können zu Problemen führen, darum vorher aus dem Cluster entfernen **oder** `data` Verzeichnis auf Master und allen Worker Nodes anlegen und jeweils auf gleichen NFS Path auf dem Rack Server mounten, siehe [Persistenz](../F/)

* [Persistent Volume und Claim](jupyter/09-5-hostPath.ipynb)
* [Volume und mehrere Container in Pod](jupyter/09-6-Volume.ipynb)

**Konfiguration und Umgebungsvariablen**

* [Secrets](jupyter/09-7-Secrets.ipynb)
* [ConfigMaps](jupyter/09-8-ConfigMap.ipynb)

### Links

* [DockerHub download rate limits](https://microk8s.io/docs/dockerhub-limits)
* [Use NFS for Persistent Volumes](https://microk8s.io/docs/nfs)



