Kubernetes
==========

Umgebung zum Kurs: [Docker und Kubernetes – Übersicht und Einsatz](https://github.com/mc-b/duk).

Dashboard
---------

Das Kubernetes Dashboard ist wie folgt erreichbar.

    https://${fqdn}:8443

Beispiele
---------

Die Umgebung beinhaltet eine Vielzahl von Beispielen als Juypter Notebooks. Die Jupyter Notebook Oberfläche ist wie folgt erreichbar:

    http://${fqdn}:32188
