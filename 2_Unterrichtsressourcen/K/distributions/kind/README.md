# Kind

Kubernetes Cluster, wo die Nodes als Docker Container laufen.

Nur bedingt brauchbar, weil die Ports, einzeln von Docker an den Host weitergeleitet werden müssen.

### Links

* [kind Doku](https://kind.sigs.k8s.io/docs)