K3s Kubernetes Distribution
---------------------------

[K3s](https://k3s.io/) ist eine hochverfügbare, zertifizierte Kubernetes-Distribution, die für Produktions-Workloads an unbeaufsichtigten, mit beschränkten Ressourcen, entfernten Standorten oder innerhalb von IoT-Appliances entwickelt wurde.

Neue VM, min. 2 GB RAM und 16 GB HD, mit [Cloud-init](cloud-init-k3s.yaml) erstellen.