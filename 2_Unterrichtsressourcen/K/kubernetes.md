## Kubernetes

Für eine Kubernetes Umgebung eignet sich [microk8s](https://microk8s.io/docs) von Ubuntu.

Um alle Beispiele durchspielen zu können, braucht es eine VM mit min. 8 GB RAM, 2 CPUs und 32 GB HD. 

Für eine einfache Kubernetes Umgebung oder eine Worker Node, ohne Dashboard, Jupyter Notebooks sind die Befehle hinter `sudo snap install kubectl` wegzulassen. Dann kann der RAM Speicher halbiert werden.

Das Cloud-init (für Ubuntu) Script ist wie folgt:

    #cloud-config
    users:
      - name: ubuntu
        sudo: ALL=(ALL) NOPASSWD:ALL
        groups: users, admin
        home: /home/ubuntu
        shell: /bin/bash
        lock_passwd: false
        plain_text_passwd: 'password'        
    # login ssh and console with password
    ssh_pwauth: true
    disable_root: false    
    packages:
      - unzip
    runcmd:
      - sudo snap install microk8s --classic
      - sudo usermod -a -G microk8s ubuntu
      - sudo microk8s enable dns ingress
      - sudo mkdir -p /home/ubuntu/.kube
      - sudo microk8s config >/home/ubuntu/.kube/config
      - sudo chown -f -R ubuntu /home/ubuntu/.kube
      - sudo snap install kubectl --classic   
      - sudo microk8s kubectl apply -f https://raw.githubusercontent.com/mc-b/duk/master/addons/dashboard-skip-login-no-ingress.yaml
      - su - ubuntu -c "git clone https://gitlab.com/ch-tbz-hf/Stud/cnt.git"
      - sudo mkdir /data
      - sudo chown ubuntu:ubuntu /data
      - su - ubuntu -c "cp -rp cnt/2_Unterrichtsressourcen/K/jupyter /data"
      - sudo microk8s kubectl apply -f https://raw.githubusercontent.com/mc-b/lernkube/master/data/DataVolume.yaml
      - sudo microk8s kubectl apply -f https://raw.githubusercontent.com/mc-b/duk/master/jupyter/jupyter-base-microk8s.yaml         
      
**Erweiterungen**

Die letzten sieben Befehle 
* Startet das Dashboard, erreichbar mittels https://*ip-vm*:8443
* Richtet einen Persistenten Speicher, im Verzeichnis `/data` ein
* Startet [Jupyter Notebooks](https://jupyter-tutorial.readthedocs.io/de/latest/index.html) Umgebung. Die Jupyter Umgebung ist via http://*ip-vm*:32188 erreichbar.

**Hinweis**: einfacher geht es mit Terraform. Die benötigten Deklarationen befinden sich in diesem Verzeichnis. Inkl. Cloud-init Scripts für [Master](cloud-init-microk8smaster.yaml) und [Worker](cloud-init-microk8sworker.yaml). 

**Testen der Umgebung auf der Kommandozeile**

Anmelden mittels ssh (Mac, Linux) oder putty, bitvise (Windows). User: `ubuntu`, Password `password`.

Einen Apache Web Server starten:

    kubectl apply -f https://raw.githubusercontent.com/mc-b/duk/master/test/apache.yaml
    
Der Port 80 des Webservices wird von Kubernetes wird automatisch auf 32xxx gemappt und kann wie folgt angezeigt werden:

    kubectl get services
    
Die IP-Adresse der VM und der anzeigte Port ergibt den URL wo der Apache Webserver läuft.
      
