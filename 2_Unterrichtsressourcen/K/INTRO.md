Kubernetes
==========

Kubernetes Cluster
------------------

Mit Master verbinden und `microk8s add-node` ausführen. Die Ausgabe ist dann jeweils, mittels Voranstellung von `sudo`, auf dem Worker auszuführen.

    ssh ubuntu@${ip}
    
    microk8s add-node --token-ttl 3600
    exit
    
    ssh ubuntu@${ip_01}
    sudo <Ausgabe von oben>
    exit
    
Die obigen Befehle sind für jeden Worker zu wiederholen.  

Zusätzlich müssen sich die Worker, via NFS, mit dem Master verbinden. Ansonsten funktionieren die Beispiele, die Persistenz verwenden, nicht.

    sudo mount -t nfs ${ip}:/data /data    

Kubernetes Dashboard
--------------------    

    https://${ip}:8443

Ein Token ist nicht nötig, einfach "Überspringen" drücken. 


Container und Service Discovery
-------------------------------

* [Pods und Services (kubectl-Variante)](http://${ip}:32188/notebooks/work/09-1-kubectl.ipynb)
* [Pods und Services (YAML-Variante)](http://${ip}:32188/notebooks/work/09-2-YAML.ipynb)
* [Ingress (Reverse Proxy)](http://${ip}:32188/notebooks/work/09-2-Ingress.ipynb)

Lastverteilung und Selbstheilung
--------------------------------

* [ReplicaSet](http://${ip}:32188/notebooks/work/09-3-ReplicaSet.ipynb)

Persistenz
----------

* [Persistent Volume und Claim](http://${ip}:32188/notebooks/work/09-5-hostPath.ipynb)
* [Volume und mehrere Container in Pod](http://${ip}:32188/notebooks/work/09-6-Volume.ipynb)

Worker Nodes können zu Problemen führen, darum vorher aus dem Cluster entfernen **oder** `data` Verzeichnis auf Master und allen Worker Nodes anlegen und jeweils auf gleichen NFS Path auf dem Rack Server mounten, siehe Persistenz - Kapitel F

Konfiguration und Umgebungsvariablen
------------------------------------

* [Secrets](http://${ip}:32188/notebooks/work/09-7-Secrets.ipynb)
* [ConfigMaps](http://${ip}:32188/notebooks/work/09-8-ConfigMap.ipynb)

Mehr Informationen siehe [README.md](README.md)    

Continuous Delivery
-------------------

* [Rolling Update](http://${ip}:32188/notebooks/work/09-4-Deployment.ipynb)
* [Blue/Green Deployments](http://${ip}:32188/notebooks/work/09-4-Deployment-BlueGreen.ipynb)
* [Canary Deployments](http://${ip}:32188/notebooks/work/09-4-Deployment-Canary.ipynb)

Mehr Informationen siehe [README Kapitel L](../L/README.md)    

