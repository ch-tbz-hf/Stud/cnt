# AWS EKS 

Kubernetes Cluster in der AWS Cloud basierend auf EKS.

Funktioniert nur mit AWS Academy, da Role und Subnetze statisch sind.

**ACHTUNG**: Cluster immer wieder zerstören. Ansonsten werden auch nach `End Lab` hohe Kosten berechnet, weil der Cluster nicht herunterfährt.

## Vorbereitung

In AWS Academy einloggen und `Start Lab` ausführen.

AWS CLI installieren, Zugriffsinformationen aus Tab `AWS Details` auslesen und als Datei `config.txt` im aktuellen Verzeichnis speichern.

Umgebungsvariable `AWS_CONFIG_FILE` auf `config.txt` setzen und evtl. Dateien im Homeverzeichnis `.aws` löschen.

Wechseln in `AWS` Web-UI und Id des IAM Users `LabRole` und von zwei Subnetzen auslesen und in `variables.tf` eintragen.

## Cluster anlegen

    terraform init
    terraform apply
    
## Feintuning

Die Worker Nodes verwenden die Standard Kubernetes Security Group. Diese ist zu ändern auf die neu erstellte Security Group. 

## Kube Config Datei holen

Datei `kubectl` lokal installieren und Konfigurationsdatei `~/.kube/config`, für Kubernetes, erstellen bzw. von AWS auslesen:

    aws eks --region us-east-1 update-kubeconfig --name base

    
