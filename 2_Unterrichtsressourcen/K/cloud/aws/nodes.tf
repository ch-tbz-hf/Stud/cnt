#
#   Worker Nodes

resource "aws_eks_node_group" "education" {
  cluster_name    = aws_eks_cluster.education.name
  node_group_name = var.module
  node_role_arn   = var.role

  subnet_ids = var.subnet_ids

  scaling_config {
    desired_size = 1
    max_size     = 2
    min_size     = 1
  }

  update_config {
    max_unavailable = 1
  }
}