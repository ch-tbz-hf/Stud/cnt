#
#   Kubernetes Cluster

resource "aws_eks_cluster" "education" {
  name     = var.module
  role_arn = var.role

  vpc_config {
    subnet_ids                = var.subnet_ids
    #cluster_security_group_id = aws_security_group.security.id
  }
}

output "endpoint" {
  value = aws_eks_cluster.education.endpoint
}

output "kubeconfig-certificate-authority-data" {
  value = aws_eks_cluster.education.certificate_authority[0].data
}