#
# Variablen

# Allgemeine Variablen

variable "module" {
  description = "Modulname: wird als Hostname verwendet"
  type        = string
  default     = "base"
}

variable "ports" {
  description = "Ports welche in der Firewall geoeffnet sind"
  type        = list(number)
  default     = [22, 80, 443, 16443, 25000]
}

variable "userdata" {
  description = "Cloud-init Script"
  type        = string
  default     = "cloud-init.yaml"
}

# AWS spezifische Variablen

variable "role" {
  description = "Id User LabRole"
  type        = string
  default     = "arn:aws:iam::532246636370:role/LabRole"
}

variable "subnet_ids" {
  description = "Subnetze"
  type        = list(any)
  default     = ["subnet-0243b9c0e43bb6722", "subnet-0ac24e4fa21a0ced3"]
}

