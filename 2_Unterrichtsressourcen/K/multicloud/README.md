MultiCloud Kubernetes
---------------------

Erstellt 
* einen Master in der Azure Cloud
* und je einen Worker in der AWS und im MAAS.

Um das Beispiel zu starten ist wie folgt vorzugehen:

    az login -u <user>
    aws configure                   # oder AWS_CONFIG_FILE setzen
    
    terraform init
    terraform apply
    
Vorgängig sind die Cloud-init Dateien, der Worker, um die WireGuard Konfiguration zu ergänzen.    
    
Anschliesend ist wie in [INTRO](INTRO.md) beschrieben, die Worker mit der Master Node zu verbinden.

Zum Testen mit dem Master verbinden und folgendes eingeben:

    kubectl get nodes
    
Alternativ kann das Dashboard verwendet werden.


Zu Beachten
-----------

microk8s braucht eindeutige DNS Namen für den Master und die Worker-Nodes. Deshalb werden diese in /etc/hosts vom Cloud-init Script eingetragen.

Trotzdem, kann es sein, dass der Cluster nicht sauber läuft. Dann sind zuerst alle VPN-IP Adresse in alle /etc/hosts einzutragen und dann `microk8s add-node` zu versuchen.

Alle VMs müssen sich im gleichen (VPN) Netzwerk befinden.

