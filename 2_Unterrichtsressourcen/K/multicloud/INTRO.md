Kubernetes
==========

Kubernetes Cluster
------------------

Mit Master verbinden und `microk8s add-node` ausführen. Die Ausgabe ist dann jeweils, mittels Voranstellung von `sudo`, auf dem Worker auszuführen.

    ssh ubuntu@${ip}
    
    kubeadm sudo kubeadm token create --print-join-command
    exit
    
    ssh ubuntu@${ip_01}
    sudo <Ausgabe von oben>
    exit
    
    
Die obigen Befehle sind für jeden Worker zu wiederholen, also auch für ${ip_02}.

Zusätzlich müssen sich die Worker, via NFS, mit dem Master verbinden. Ansonsten funktionieren die Beispiele, die Persistenz verwenden, nicht.

    sudo mount -t nfs ${ip}:/data /data    

Kubernetes Dashboard
--------------------    

    https://${ip}:8443

Ein Token ist nicht nötig, einfach "Überspringen" drücken. 
