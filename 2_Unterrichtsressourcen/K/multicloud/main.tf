###
#   Multicloud Kubernetes - funktioniert nicht mit microk8s (Problem Namensaufloesung)


# MAAS
module "master" {
  source     = "git::https://github.com/mc-b/terraform-lerncloud-maas"
  module     = "master-${var.host_no}"
  userdata   = "cloud-init-k8smaster.yaml"
  memory     = 4
  cores      = 2
  storage    = 20  
  ports      = [ 22, 80, 16443, 25000 ]
  
  # Server Access Info
  url      = "${var.url}"
  key      = "${var.key}"
  vpn      = "${var.vpn}"    
}

# Azure Cloud
module "worker-01" {
  source     = "git::https://github.com/mc-b/terraform-lerncloud-azure"
  module     = "worker-01"
  userdata   = "cloud-init-k8sworker-01.yaml"
  memory     = 4
  cores      = 2
  storage    = 20  
  ports      = [ 22, 80, 16443, 25000 ]
}

# AWS Cloud
module "worker-02" {
  source     = "git::https://github.com/mc-b/terraform-lerncloud-aws"
  module     = "worker-02"
  userdata   = "cloud-init-k8sworker-02.yaml"
  memory     = 4
  cores      = 2
  storage    = 20  
  ports      = [ 22, 80, 16443, 25000 ]
}

