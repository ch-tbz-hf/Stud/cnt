# Zugriffs Informationen
#
# Als Umgebungsvariablen TF_VAR_<name> ablegen
# TF_VAR_url=https://10.6.37.8:5240/MAAS

variable "url" {
  description = "Evtl. URL fuer den Zugriff auf das API des Racks Servers"
  type        = string
}

# Umgebungsvariable TF_VAR_key ablegen
variable "key" {
  description = "API Key, Token etc. fuer Zugriff"
  type        = string
  sensitive   = true
}

variable "vpn" {
  description = "Optional VPN welches eingerichtet werden soll"
  type        = string
}

variable "host_no" {
    description = "Host-No fuer die erste Host-IP Nummer"
    type    = number
    default = 30
}