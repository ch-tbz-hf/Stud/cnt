###
#   Testumgebung Kubernetes 

module "master" {
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-multipass"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-aws"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-azure"
  source     = "git::https://github.com/mc-b/terraform-lerncloud-maas"
  module     = "master-${var.host_no}-${terraform.workspace}"
  userdata   = "cloud-init-microk8smaster.yaml"
  memory     = 4
  cores      = 2
  storage    = 20  
  ports      = [ 22, 80, 16443, 25000, 2049 ]
  
  # Server Access Info
  url      = "${var.url}"
  key      = "${var.key}"
  vpn      = "${var.vpn}"   
}

module "worker-01" {
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-multipass"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-aws"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-azure"
  source     = "git::https://github.com/mc-b/terraform-lerncloud-maas"
  module     = "worker-${var.host_no + 1}-${terraform.workspace}"
  userdata   = "cloud-init-microk8sworker.yaml"
  memory     = 4
  cores      = 2
  storage    = 20  
  ports      = [ 22, 80, 16443, 25000, 2049 ]
  
  # Server Access Info
  url      = "${var.url}"
  key      = "${var.key}"
  vpn      = "${var.vpn}"   
}

/*
module "worker-02" {
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-multipass"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-aws"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-azure"
  source     = "git::https://github.com/mc-b/terraform-lerncloud-maas"
  module     = "worker-${var.host_no + 2}-${terraform.workspace}"
  userdata   = "cloud-init-microk8sworker.yaml"
  memory     = 4
  cores      = 2
  storage    = 20  
  ports      = [ 22, 80, 16443, 25000, 2049 ]
  
  # Server Access Info
  url      = "${var.url}"
  key      = "${var.key}"
  vpn      = "${var.vpn}"   
}
*/