Windows-Subsystem für Linux mit Kubernetes
------------------------------------------

Das [Windows-Subsystem für Linux](https://docs.microsoft.com/de-de/windows/wsl/) bzw. im englischen Originaltitel Windows Subsystem for Linux, kurz WSL, ist eine Kompatibilitätsschicht zum Ausführen von Linux-Executables im ELF-Format in Windows 10. 

Nach der Aktivierung von [WSL2](https://docs.microsoft.com/de-de/windows/wsl/install) und Installation von Ubuntu (ab 20.04) kann Kubernetes wie folgt installiert werden:

    # Install the required packages for SystemD
    apt install -yqq fontconfig daemonize
    # Creates a default user and adds it to the sudo group
    useradd -m -s /bin/bash -G sudo ubuntu
    # Reset the password of the default user
    passwd ubuntu  
    
    # Edit the sudoers to remove the password request
    visudo
        %sudo   ALL=(ALL:ALL) NOPASSWD: ALL      


    # Create the wsl.conf file
    cat <<EOF >/etc/wsl.conf
    [automount]
    enabled = true
    options = "metadata,uid=1000,gid=1000,umask=22,fmask=11,case=off"
    mountFsTab = true
    crossDistro = true
    
    [network]
    generateHosts = false
    generateResolvConf = true
    
    [interop]
    enabled = true
    appendWindowsPath = true
    
    [user]
    default = ubuntu
    EOF
   
**Hinweis**: die Installation geht davon aus, dass wir einen Standard User `ubuntu` eingerichtet haben.

**SystemD-Setup**

Eine der Hauptlücken von WSL ist (war?), dass SystemD aufgrund des Fehlens von pid 1 nicht funktioniert.

Dadurch können einige Anwendungen, die von SystemD abhängen nicht gestartet werden, bzw. bei Ubuntu `snap` nicht verwendet werden.

Glücklicherweise gibt es eine Lösung [Running Snaps on WSL2 ](https://forum.snapcraft.io/t/running-snaps-on-wsl2-insiders-only-for-now/13033), wie wie folgt verwendet werden kann:

    # Create the starting script for SystemD
    cat <<EOF >/etc/profile.d/00-wsl2-systemd.sh
    SYSTEMD_PID=$(ps -ef | grep '/lib/systemd/systemd --system-unit=basic.target$' | grep -v unshare | awk '{print $2}')
    
    if [ -z "$SYSTEMD_PID" ]; then
       sudo /usr/bin/daemonize /usr/bin/unshare --fork --pid --mount-proc /lib/systemd/systemd --system-unit=basic.target
       SYSTEMD_PID=$(ps -ef | grep '/lib/systemd/systemd --system-unit=basic.target$' | grep -v unshare | awk '{print $2}')
    fi
    
    if [ -n "$SYSTEMD_PID" ] && [ "$SYSTEMD_PID" != "1" ]; then
        exec sudo /usr/bin/nsenter -t $SYSTEMD_PID -a su - $LOGNAME
    fi
    EOF
    
Nach den Änderungen ist das WSL neu zu starten und mittels `pstree -p -n -T -A` sollten wir jetzt SystemD als Prozess 1 sehen.

**Netzwerkeinrichtung** 

Bei SystemD treten möglicherweise einige Störungen auf Netzwerkebene auf. Einer ist, dass der DNS-Resolver nicht funktioniert. Um das Problem zu beheben, aktualisieren wir die resolved.confDatei so, dass sie ein öffentliches DNS verwendet:    
    
    sudo vi /etc/systemd/resolved.conf
    ...
    [Resolve]
    DNS=8.8.8.8
    
    sudo systemctl restart systemd-resolved
    sudo apt update
    
    # Forward all localhost ports to default interface
    echo 'net.ipv4.conf.all.route_localnet = 1' | sudo tee -a /etc/sysctl.conf
    # Apply the change
    sudo sysctl -p /etc/sysctl.conf  
    
**Microk8s (Kubernetes) einrichten**

`snap` sollte jetzt funktionieren und wir können microk8s einrichten:

    # Install the "latest/stable" version
    sudo snap install microk8s --classic
    # Let's enable the DNS and Ingress (Reverse Proxy) addons
    microk8s.enable dns ingress
    # Check the status from Microk8s > this might take 1 or 2 minutes
    sudo microk8s.status
    # Check the Kubernetes version installed (client & server)
    sudo microk8s.kubectl version
    # Check that the cluster is running correctly
    sudo microk8s.kubectl cluster-info  
    
    # Add the user to the microk8s group, install kubectl
    sudo snap install kubectl --classic
    sudo usermod -a -G microk8s ubuntu
    sudo mkdir -p /home/ubuntu/.kube
    sudo microk8s config | sudo tee  /home/ubuntu/.kube/config
    sudo chown -f -R ubuntu:ubuntu /home/ubuntu/.kube    

### Links

* [Kubernetes on Windows with MicroK8s and WSL 2](https://ubuntu.com/blog/kubernetes-on-windows-with-microk8s-and-wsl-2)
* [WSL2+Microk8s: the power of multinodes](https://wsl.dev/wsl2-microk8s/)