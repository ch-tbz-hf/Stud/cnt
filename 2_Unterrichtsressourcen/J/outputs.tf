###
#   Outputs wie IP-Adresse und DNS Name
#  

output "ip_vm" {
  value = module.microservices.ip_vm
  description = "The IP address of the server instance."
}

output "fqdn_vm" {
  value = module.microservices.fqdn_vm
  description = "The FQDN of the server instance."
}

# Einfuehrungsseite(n)

output "README" {
  value = templatefile( "INTRO.md", { ip = module.microservices.ip_vm, fqdn = module.microservices.fqdn_vm } )
} 
