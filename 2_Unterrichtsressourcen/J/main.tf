###
#   Testumgebung Kubernetes inkl. Microservices Beispiele 

module "microservices" {
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-multipass"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-aws"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-azure"
  source     = "git::https://github.com/mc-b/terraform-lerncloud-maas"
  module     = "microservices-${var.host_no}-${terraform.workspace}"
  userdata   = "cloud-init-microk8smaster.yaml"
  memory     = 8
  cores      = 4
  storage    = 32
  
  # Server Access Info
  url      = "${var.url}"
  key      = "${var.key}"
  vpn      = "${var.vpn}"    
}