Architekturstyles für Container (Microservices)
===============================================

Synchrone Microservices
-----------------------

    http://${ip}:32188/notebooks/work/demo/Microservices-REST.ipynb
    
    
Asynchrone Microservices
------------------------

    http://${ip}:32188/notebooks/work/demo/Microservices-Messaging.ipynb
    

Kubernetes Dashboard
--------------------    

    https://${ip}:8443

Ein Token ist nicht nötig, einfach "Überspringen" drücken.
        
Mehr Informationen siehe [README.md](README.md)    