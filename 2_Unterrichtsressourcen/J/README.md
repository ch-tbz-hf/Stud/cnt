## Architekturstyles für Container (Microservices)

[[_TOC_]]

### Microservices


[![](https://img.youtube.com/vi/PH4HtZ8naWs/0.jpg)](https://www.youtube.com/watch?v=PH4HtZ8naWs)

Microservices YouTube Einführung

---

Einer der grössten Anwendungsfälle und die stärkste treibende Kraft hinter dem Aufstieg von Containern sind Microservices.

Microservices sind ein Weg, Softwaresysteme so zu entwickeln und zu kombinieren, dass sie aus kleinen, unabhängigen Komponenten bestehen, die untereinander über das Netz interagieren. Das steht im Gegensatz zum klassischen, monolithischen Weg der Softwareentwicklung, bei dem es ein einzelnes, grosses Programm gibt.

Wenn solch ein Monolith dann skaliert werden muss, kann man sich meist nur dazu entscheiden, vertikal zu skalieren (scale up), zusätzliche Anforderungen werden in Form von mehr RAM und mehr Rechenleistung bereitgestellt. Microservices sind dagegen so entworfen, dass sie horizontal skaliert werden können (scale out), indem zusätzliche Anforderungen durch mehrere Rechner verarbeitet werden, auf die die Last verteilt werden kann.

In einer Microservices-Architektur ist es möglich, nur die Ressourcen zu skalieren, die für einen bestimmten Service benötigt werden, und sich damit auf die Flaschenhälse des Systems zu beschränken. In einem Monolith wird alles oder gar nichts skaliert, was zu verschwendeten Ressourcen führt.

### Microservice Umgebung 

Für den Betrieb der nachfolgenden Microservices brauchen wir eine Kubernetes Umgebung.

Dazu erstellen wir eine VM mit mindestens 8 GB RAM, 2 CPUs und 32 GB HD

Das Cloud-init Script, ist [hier](cloud-init-microk8smaster.yaml).

### Synchrone Microservices

![](https://github.com/mc-b/duk/raw/e85d53e7765f16833ccfc24672ae044c90cd26c1/data/jupyter/demo/images/Microservices-REST.png)

Quelle: Buch Microservices Rezepte
- - -

Das Beispiel besteht aus drei Microservices: **Order**, **Customer** und **Catalog**.

Order nutzt Catalog und Customer mit der REST-Schnittstelle. Ausserdem bietet jeder Microservice einige HTML-Seiten an.

Zusätzlich ist im Beispiel ein Apache-Webserver (Menu und Reverse Proxy) installiert, der dem Benutzer mit einer Webseite einen einfachen Einstieg in das System ermöglicht.

### Asynchrone Microservices    

![](https://github.com/mc-b/duk/raw/e85d53e7765f16833ccfc24672ae044c90cd26c1/data/jupyter/demo/images/Microservices-Messaging.png)

Quelle: Buch Microservices Rezepte
- - -

Das System besteht aus einem Microservice **order**, der eine Bestellung über die Weboberfläche entgegennimmt.

Die Bestellung schickt der Bestellprozess dann als Record über Kafka an den Microservice für den Versand **shipping** und den Microservice für die Erstellung der Rechnung **invoicing**.

Die Bestellung wird als JSON übertragen. So können der Rechnungs-Microservice und der Versand-Microservice aus der Datenstruktur jeweils die Daten auslesen, die für den jeweiligen Microservice relevant sind.

Der Versand-Microservice und der Rechnungs-Microservice speichern die Informationen aus den Records in ihren eigenen Datenbank-Schemata. Alle Microservices nutzen eine gemeinsame Postgres-Datenbank.

### Hands-On

* [Synchrone Microservices](../K/jupyter/demo/Microservices-REST.ipynb)
* [Asynchrone Microservices](../K/jupyter/demo/Microservices-Messaging.ipynb)
* [Service Mesh](../K/jupyter/demo/Microservices-Istio.ipynb) mit [Istio](https://istio.io/) basierend auf [Microservice Istio Sample](https://github.com/ewolff/microservice-istio)
