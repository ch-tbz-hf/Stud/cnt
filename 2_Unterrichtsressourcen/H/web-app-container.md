## Web-App mit Container

Zuerst erstellen wir eine neue VM mit Docker.

Dazu sieht das Cloud-init Script wie folgt aus:

    #cloud-config
    users:
      - name: ubuntu
        sudo: ALL=(ALL) NOPASSWD:ALL
        groups: users, admin
        home: /home/ubuntu
        shell: /bin/bash
        lock_passwd: false
        plain_text_passwd: 'password'        
    # login ssh and console with password
    ssh_pwauth: true
    disable_root: false    
    packages:
      - docker.io
    runcmd:
     - sudo usermod -aG docker ubuntu 

Nachdem wir in die VM, z.B. via ssh, gewechselt haben, führen wir die folgenden Befehle aus:
    
Git Repository clonen 

    git clone https://gitlab.com/ser-cal/Container-CAL-webapp_v1.git
    cd Container-CAL-webapp_v1/App
    
Einmalig müssen wir die App packetieren (d.h. in ein Container-Image verpacken), dies geschieht mittels `docker` und einem vorbereiteten `Dockerfile`. Details siehe [Container-CAL-webapp_v1](https://gitlab.com/ser-cal/Container-CAL-webapp_v1).

    docker image build -t marcellocalisto/webapp_one:1.0 .
    docker image ls
    
    > REPOSITORY                   TAG            IMAGE ID       CREATED         SIZE
    > marcellocalisto/webapp_one   1.0            f4e7bf13c7fa   6 seconds ago   195MB
    
Statt 1 GB für `NodeJS` und `npm` sind es jetzt, inkl. unserer App (Microservice), nur 195 MB.  

Nun können wir die, als Container Image, packetierte App (Microservice) starten und testen:  

    docker container run -d --name app1 -p 8080:8080 marcellocalisto/webapp_one:1.0
    curl localhost:8080
    
Wurde die App (Microservice) so gestartet, reden wir von einem Container. Die Kontrolle über den Container übernimmt das Container Runtime (`Docker`).  

Um das zu überprüfen, können wir einen Docker Befehl oder Linux Befehl verwenden:

    docker container ps
    
    > ...   marcellocalisto/webapp_one:1.0   "docker-entrypoint.s…"   ...   0.0.0.0:8080->8080/tcp, :::8080->8080/tcp   app1
    
oder wie schauen uns die Prozesshierarchie an:

    pstree -n -p -T -A
    
    > systemd(1)-+-systemd-journal(350)
    >       `-containerd-shim(4742)---sh(4763)---node(4795)     # hier läuft, gestartet vom Container Runtime containerd (Bestandteil von Docker) der Container        
    
Gehen aber auch weitere Instanzen? 

Ja, weil das Portmapping kann an das Container Runtime (Docker) delegiert werden. Die App (Microservice) muss dazu nicht verändert werden.    
    
    docker container run -d --name app2 -p 8081:8080 marcellocalisto/webapp_one:1.0
    curl localhost:8081
    
Zur Kontrolle können wir die erstellen Iptables ausgeben:

    sudo iptables -t nat -L -n
    
    > Chain POSTROUTING (policy ACCEPT)
    > MASQUERADE  tcp  --  172.17.0.2           172.17.0.2           tcp dpt:8080
    > MASQUERADE  tcp  --  172.17.0.3           172.17.0.3           tcp dpt:8080
    > Chain DOCKER (2 references)
    > DNAT       tcp  --  0.0.0.0/0            0.0.0.0/0            tcp dpt:8080 to:172.17.0.2:8080
    > DNAT       tcp  --  0.0.0.0/0            0.0.0.0/0            tcp dpt:8081 to:172.17.0.3:8080
    
Warum kann ich die App aber mehrmals starten? Nur am Portmapping kann es nicht liegen, siehe [Linux Namespaces](https://github.com/mc-b/duk/tree/master/linuxns).

Wir wechseln in den ersten Container und schauen uns die Netzwerk-Adapter an.

    docker exec -it app1 bash
    
Jetzt sind wir im Container, dort fehlen aber einige Befehle, welche wir zuerst installieren müssen. Der Hintergrund ist, dass im Container nur die Dateien seines Images sichtbar sind.

    apt update
    apt install -y iproute2 psmisc
    
Jetzt können wir uns die Netzwerk-Adapter anschauen. Wenn wir die IP, hier 172.17.0.2 mit der IP der VM vergleichen, werden wir sehen, dass diese Unterschiedlich sind.
    
    ip addr
    
    > 1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    >    inet 127.0.0.1/8 scope host lo
    > 10: eth0@if11: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
    >    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
    
Auch die Prozesshierarchie ist interessant, werden doch nur die Prozesse im Container angezeigt.

    pstree -p -T
    
    > sh(1)---node(8)
    
**Fazit**: Jeder Container verfügt, vereinfacht, über:
* ein eigenes Dateisystem (darum mussten wir zuerst SW installieren)
* einen eigenen Netzwerk-Adapter
* eine eigene Prozesshierarchie. 