###
#   Outputs wie IP-Adresse und DNS Name
#  

output "ip_vm_base" {
  value = module.base.ip_vm
  description = "The IP address of the server instance."
}

output "fqdn_vm_base" {
  value = module.base.fqdn_vm
  description = "The FQDN of the server instance."
}

output "ip_vm_docker" {
  value = module.docker.ip_vm
  description = "The IP address of the server instance."
}

output "fqdn_vm_docker" {
  value = module.docker.fqdn_vm
  description = "The FQDN of the server instance."
}

# Einfuehrungsseite(n)

output "README" {
  value = templatefile( "INTRO.md", { ip_base = module.base.ip_vm, fqdn_base = module.base.fqdn_vm, ip_docker = module.docker.ip_vm, fqdn_docker = module.docker.fqdn_vm } )
} 