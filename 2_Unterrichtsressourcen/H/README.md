## Container Standards und Technologien

[[_TOC_]]

### Open Container Initiative (OCI)

Die [Open Container Initiative](https://opencontainers.org/) ist eine offene Governance-Struktur mit dem ausdrücklichen Zweck, offene Industriestandards für das [Container Image Format](https://github.com/opencontainers/image-spec/blob/main/spec.md) und [Laufzeitumgebungen (Runtime)](https://github.com/opencontainers/runtime-spec/blob/master/spec.md) zu schaffen.

Die OCI wurde im Juni 2015 von Docker und anderen führenden Unternehmen der Containerbranche gegründet und enthält derzeit zwei Spezifikationen: die Runtime Specification (runtime-spec) und die Image Specification (image-spec). 

Die Runtime Specification beschreibt, wie ein "Container (OCI) Image" erstellt und auf die Festplatte entpackt wird. 

Und wie ein "Container (OCI) Image" heruntergeladen, entpackt und aus Container ausgeführt wird.

### Linux Namespaces

![](https://github.com/mc-b/duk/raw/master/images/linux-namespaces.png)

- - -

Ist die Technologie hinter den Containern.

Deren Aufgabe ist es die Ressourcen des Kernsystems (in diesem Falle also des Linux Kernels) voneinander zu isolieren.

**Arten von Namespaces**

* IPC -Interprozess-Kommunikation
* NET - Netzwerkressourcen
* PID -Prozess-IDs
* USER - Benutzer/Gruppen-Ids
* (UTS - Systemidentifikation): Über diesen Namespace kann jeder Container einen eigenen Host- und Domänennamen erhalten.

### Hands-on

* [Web-App ohne Container](web-app.md)
* [Web-App mit Container](web-app-container.md)
* [Linux Namespaces oder die Linux Sicht auf Container](https://github.com/mc-b/duk/tree/master/linuxns)

### Links

* [DockerHub download rate limits](https://microk8s.io/docs/dockerhub-limits)