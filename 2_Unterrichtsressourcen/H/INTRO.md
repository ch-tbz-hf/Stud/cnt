Container Standards und Technologien
====================================

Web-App ohne Container
----------------------

Beispiel Umgebung für Web-App ohne Container

Einloggen mittels

    ssh ubuntu@${ip_base}
    
Mehr Informationen und Hands-on, siehe [web-app.md](web-app.md)

    
Web-App mit Container
---------------------

Einfache Docker Umgebung.

Einloggen mittels

    ssh ubuntu@${ip_docker}
    
Mehr Informationen und Hands-on, siehe [web-app-container.md](web-app-container.md)    