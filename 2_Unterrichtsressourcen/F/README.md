## Persistenz

[[_TOC_]]

Persistenz (von lateinisch persistere „verharren, stehen bleiben“) ist in der Informatik die Eigenschaft eines Systems, den Zustand seiner Daten, seines Objektmodells und/oder seiner logischen Verbindungen über lange Zeit – insbesondere über einen geplanten oder ungeplanten Programmabbruch hinaus – bereitzuhalten. Dafür wird ein nichtflüchtiges Speichermedium benötigt; auch das Dateisystem oder eine Datenbank.

### NFS 

Das [Network File System (NFS, auch Network File Service)]() – ist ein von Sun Microsystems entwickeltes Protokoll, das den Zugriff auf Dateien über ein Netzwerk ermöglicht. Dabei werden die Dateien nicht wie z. B. bei FTP übertragen, sondern die Benutzer können auf Dateien, die sich auf einem entfernten Rechner befinden, so zugreifen, als ob sie auf ihrer lokalen Festplatte abgespeichert wären.

Bei diesem Unix-Netzwerkprotokoll handelt es sich um einen Internet-Standard (RFC 1094, RFC 1813, RFC 3530, RFC 7530), der auch als verteiltes Dateisystem (englisch distributed file system) bezeichnet wird.

Die Entsprechung zu NFS heißt unter Windows- und OS/2-Umgebungen Server Message Block (SMB). Während sich bei SMB der Benutzer authentifiziert, authentisiert das populärere NFSv3 den Client-Rechner, erst NFSv4 ermöglicht Benutzerauthentifikation. NFS-Dienste sind auch auf Microsoft-Windows-Servern verfügbar, wodurch UNIX-Workstations Zugang zu deren Dateien erhalten können.

### Installation und Konfiguration

Als VMs und Cloud-init Scripts, verwenden wir die vom Kapitel [Portmapping / Reverse Proxy](../E/), ausser für `order`. Die IP-Tables Einträge werden nicht gebraucht.

Wir wollen auf dem **Rack Server** eine Persistenze Ablage für unsere VMs einrichten. Dazu verwenden wir NFS.

Die Installation ist einfach

    sudo apt-get update
    sudo apt install -y nfs-kernel-server
    
Als Ablageort erstellen wir ein Verzeichnis `/data/storage` und legen die Zugriffsrechte fest.

    sudo mkdir -p /data /data/storage
    sudo chown -R ubuntu:ubuntu /data
    sudo chmod 777 /data/storage
    
Die Konfiguration von NFS erfolgt in der Datei `/etc/exports`. Dort ist `/data/storage` und das erlaubte Subnetz einzutragen.     
    
    cat <<%EOF% | sudo tee /etc/exports
    # /etc/exports: the access control list for filesystems which may be exported
    #               to NFS clients.  See exports(5).
    # Storage RW
    /data/storage 10.0.45.0/24(rw,sync,no_subtree_check,all_squash,anonuid=1000,anongid=1000)
    %EOF%
    
**ACHTUNG**: das Subnetz `10.0.45.0/24` muss auf das eigene Rack Subnetz angepasst werden.    

Zum Schluss, muss NFS neu gestartet werden:

    sudo exportfs -a
    sudo systemctl restart nfs-kernel-server

Damit VMs Dateien auf dem Rack Server ablegen können, ist das Cloud-init Script zu erweitern. Dieses sieht für `order` dann so aus:

    #cloud-config
    packages:
      - apache2 
      - nfs-client
    write_files:
     - content: |
        <html>
         <body>
          <h1>Order</h1>
          <p><a href="/order/data/test.html">Daten</a></p>
         </body>
        </html>
       path: /var/www/html/index.html
       permissions: '0644'
    runcmd:
      - sudo mkdir /var/www/html/data
      - sudo chown ubuntu:ubuntu /var/www/html/data
      - sudo mount -t nfs 10.0.45.8:/data/storage /var/www/html/data
      
**ACHTUNG**: Auf der letzten Zeile, mountet die VM das Verzeichnis `/data/storage` des Racks Server. Die IP-Adresse `10.0.45.8` ist, für das eigene Rack, anzupassen.      

Wenn wir uns jetzt mit dem Rack Server verbinden, dort im Verzeichnis `/data/storage` eine Datei anlegen, muss diese via Reverse Proxy sichtbar sein.

Beispiel:

    ssh ubuntu@10.0.45.8
    cat <<%EOF% >/data/storage/test.html
        <html>
         <body>
          <h1>Testseite</h1>
          <p>Ist auf dem Rackserver gespeichert, wird aber mittels VM order angesprochen.</p>
         </body>
        </html>
    %EOF%
    
Aufruf des Reverse Proxy, bzw. der Order VM

    http://<IP Reverse Proxy/order/data/test.hml
    
**Hinweis**: um die NFS Verbindung auch nach dem Reboot der VM verfügbar zu haben, sollte der Mount in die Datei `/etc/fstab`, in der VM, eingetragen werden.    

### Hands-on

* [Borg Backup](borg-backup.md)

### Links

* [NFS - Ubuntu Wiki](https://wiki.ubuntuusers.de/NFS/)
* [lernMAAS - Gemeinsame Dateiablage](https://github.com/mc-b/lernmaas/blob/master/doc/MAAS/Install.md#gemeinsame-datenablage-optional)
* [Borg Backup](https://borgbackup.readthedocs.io/)