## BorgBackup

BorgBackup (kurz: Borg) ist ein deduplizierendes Backup-Programm. Optional unterstützt es Komprimierung und authentifizierte Verschlüsselung.

Das Hauptziel von Borg ist es, eine effiziente und sichere Möglichkeit zur Datensicherung bereitzustellen. Die verwendete Datendeduplizierungstechnik macht Borg für tägliche Backups geeignet, da nur Änderungen gespeichert werden. Durch die authentifizierte Verschlüsselungstechnik ist Borg auch für Backups auf nicht vertrauenswürdige Ziele geeignet (z.B. Cloud Speicher).

## Beispiel

Für ein Beispiel wollen wir die Daten vom Rack Server (`/data`) jeweils auf eine VM in der Azure und AWS Cloud sichern.

Dazu legen wir in der Azure und AWS Cloud je eine VM an und Verwenden dazu das Cloud-init Script vom [Kapitel D](../D/vpn-install.md). Als SSH-Key wird der Standard-Key (siehe MS Teams) verwendet. Sind wir nicht in dessen Besitz, sollte der Public SSH-Key vom Rack Server verwendet werden.

**ACHTUNG**: pro Cloud VM eine anderen WireGuard IP-Adresse verwenden und den UDP Port vom WireGuard öffnen, im Cloud-init Script vom Kapitel D, Port 51200.

Als letztes Verbinden wir uns mittels ssh mit dem Rack Server und Testen die Verbindung mittels `ping` und `ssh`.

Wir gehen von folgender Konfiguration aus:
* Rack Server - 10.6.38.8
*  Azure VM - 10.6.38.99
* AWS VM - 10.6.38.98

Als erstes Verbinden wir uns mit dem Rack Server und von dort mit den VMs in der Cloud. Installieren Borg Backup und legen die Backup Verzeichnisse an.

    ssh 10.6.38.8
    
    ping 10.6.38.99 # VM in der Azure Cloud
    ssh 10.6.38.99
    sudo apt-get install -y borgbackup
    mkdir backup
    exit
    
    ping 10.6.38.98 # VM in der AWS Cloud
    ssh 10.6.38.98
    sudo apt-get install -y borgbackup
    mkdir backup
    exit

Wenn die Verbindung klappt, sind jetzt die Backup Verzeichnisse eingerichtet und wir können diese Initialisieren.

Durch das Setzen der wichtigsten Argumente als Umgebungsvariablen können wir uns Tiparbeit sparen.

Die Varialbe `BORG_RSH` legt fest, wie wir auf die entfernten VMs zugreifen und mit welchem SSH-Key. Dieser ist Anzupassen, wenn nicht der Standard Key (siehe MS Teams) verwendet wird.
`BORG_PASSPHRASE` legt als weiteres Sicherheitskriterium ein optionales Password fest.
`REPO_AZURE` und `REPO_AWS` legen die Ziel-VMs fest. 

    export BORG_RSH="ssh -i ~/.ssh/id_public -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o BatchMode=yes -o LogLevel=error"
    export BORG_PASSPHRASE=""       # wir verzichten auf ein weiteres Password
    export REPO_AZURE=ssh://ubuntu@10.6.38.99:22/home/ubuntu/backup 
    export REPO_AWS=ssh://ubuntu@10.6.38.98:22/home/ubuntu/backup
    
Einmalig müssen die Backup Repositories initialisiert werden. Mittels `-e keyfile` wird lokal ein Key angelegt, welcher für die Verschlüsselung der Backups verwendet wird. Ohne diesen können die Daten der Backup Repositories nicht entschlüsselt werden.  
    
    # keyfile: stores the (encrypted) key into ~/.config/borg/keys/
    borg init $REPO_AZURE -e keyfile
    borg init $REPO_AWS -e keyfile 
       
Anschliessend kann der eigentliche Backup Vorgang erfolgen. `backup01` steht dabei für den Namen des Backups.  
    
    borg create -svp -C lz4 $REPO_AZURE::backup01 /data   
    borg create -svp -C lz4 $REPO_AWS::backup01 /data
    
Die Backups und deren Inhalte können wir uns anzeigen lassen:

    borg list $REPO_AZURE
    borg list $REPO_AZURE::backup01
    
## Weitergehende Möglichkeiten

Zwei weitere Backups zu Testen anlegen

    borg create -svp -C lz4 $REPO_AZURE::backup02 /data
    borg create -svp -C lz4 $REPO_AZURE::backup03 /data
    
Die Kompression mit lz4 ist äusserst schnell aber ineffizient. Besser sind gzip und lzma, brauchen aber massiv mehr Zeit.

Backups anzeigen

    borg list $REPO_AZURE

Um, z.B. einzelne Dateien wiederherzustellen, kann ein Backup Repository, wie ein normales Dateisystem gemountet werden.    
    
    mkdir borg
    borg mount $REPO_AZURE::backup03 borg
    find borg
    umount borg
    rmdir borg
    
Damit die Backup Repositories nicht ungebremst wachsen, können unnötige oder nicht mehr benötigte Backups entfernt werden.

    borg prune --keep-daily=2 --keep-monthly=1 --prefix=backup $REPO_AZURE
    borg list $REPO_AZURE
    
Der obige Befehl behält nur die jeweils aktuellste Sicherung der letzten beiden Tage plus die des letzten Monats. D.h. die aktuellste Sicherung von heute, die aktuellste Sicherung von gestern und die aktuellste Sicherung vom Vormonat. Da alle 3 Sicherungen heute gemacht wurden, verbleibt nur backup03.

        
       
