Cloud-init Hands-on
--------------------

[[_TOC_]]

Cloud-init übernimmt die Initialisierung einer Cloud-Instanz. Es wird von allen grossen öffentlichen Cloud Anbietern, Bereitstellungssystemen für private Cloud
Infrastrukturen und Bare Metal Installationen unterstützt.

Mit Cloud-init lassen u.a. folgende Einstellungen vornehmen:
* Festlegen eines Standardgebietsschemas
* Hostname einstellen
* ssh-private Schlüssel generieren
* ssh-Schlüssel zu den .ssh/authorized_keys des Benutzers hinzufügen, damit er sich anmelden kann

Das Verhalten von cloud-init kann über, das Feld `User data` (oder `Custom data`) konfiguriert werden. `User data` wird vom Benutzer zum Startzeitpunkt an cloud-init an den Cloud-Anbieter übergeben, normalerweise als Parameter in der CLI, Vorlage oder im Cloud-Portal, die zum Starten einer Instanz verwendet werden.

Beginnt das Feld `User data` mit `#cloud-config` erkennt die Cloud Umgebung, dass es sich um Cloud-init-Anweisungen handelt.

### Die wichtigsten Einträge sind:

* **users**: Dient zum Erstellen und konfigurieren von Usern.
* **packages**: Beschreibt, in einem Array, welche Linux Packages installiert werden sollen.
* **runcmd**: Beschreibt, in einem Array, welche Shell (sh) Befehle ausgeführt werden sollen.
* **write_files**: dient zum Erstellen von (Konfigurations-)Dateien.

### Beispiel

    #cloud-config
    users:
      - name: ubuntu
        sudo: ALL=(ALL) NOPASSWD:ALL
        groups: users, admin
        home: /home/ubuntu
        shell: /bin/bash
        lock_passwd: false
        plain_text_passwd: 'password' 
        ssh_import_id:
         - gh:mc-b
        ssh_authorized_keys:
          - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDPvLEdsh/Vpu22zN3M/lmLE8zEO1alk/aWzIbZVwXJYa1RbNHocyZlvE8XDcv1WqeuVqoQ2DPflkQxdrbp2G08AWYgPNiQrMDkZBHG4GlU2Jhe9kCRiWVx/oVDeK8v3+w2nhFt8Jk/eeQ1+E19JlFak1iYveCpHqa68W3NIWj5b10I9VVPmMJVJ4KbpEpuWNuKH0p0YsUKfTQdvrn42fz5jYS1aV7qCCOOzB3WC833QRy04iHZObxDWIi/IFeIp1Gw2FkzPhoZyx4Fk9bsXfm301IePp9cwzArI2LdcOhwEZ3RW2F7ie2WJlVy5tzJjMGCaE1tZTjiCahLNEeTiTQp public-key@cloud.tbz.ch                   
    # login ssh and console with password
    ssh_pwauth: true
    disable_root: false    
    packages:
      - unzip
    runcmd:
      - echo "Hello from Cloud-init"
    write_files:
     - content: |
        Cloud-init write_files
       path: /etc/issue
       permissions: '0644'   
       
#### Einträge im Detail

##### users

    #cloud-config
    users:
      - name: ubuntu
        sudo: ALL=(ALL) NOPASSWD:ALL
        groups: users, admin
        home: /home/ubuntu
        shell: /bin/bash
        lock_passwd: false
        plain_text_passwd: 'password' 
    # login ssh and console with password
    ssh_pwauth: true
    disable_root: false  

Legt einen **User** `ubuntu` mit Password `password` an. Die oberen Einträge entsprechen der Datei `/etc/passwd`. Ausserdem wird die Zugriff via Password freigeschaltet.

**Aus Sicherheitsgründen sollte auf Passwörter verzichtet und stattdessen ssh-Keys verwendet werden.** 

Dann sieht die Cloud-init Datei wie folgt aus:

    #cloud-config
    users:
      - name: ubuntu
        sudo: ALL=(ALL) NOPASSWD:ALL
        groups: users, admin
        shell: /bin/bash
        ssh_import_id:
         - gh:mc-b
        ssh_authorized_keys:
          - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDPvLEdsh/Vpu22zN3M/lmLE8zEO1alk/aWzIbZVwXJYa1RbNHocyZlvE8XDcv1WqeuVqoQ2DPflkQxdrbp2G08AWYgPNiQrMDkZBHG4GlU2Jhe9kCRiWVx/oVDeK8v3+w2nhFt8Jk/eeQ1+E19JlFak1iYveCpHqa68W3NIWj5b10I9VVPmMJVJ4KbpEpuWNuKH0p0YsUKfTQdvrn42fz5jYS1aV7qCCOOzB3WC833QRy04iHZObxDWIi/IFeIp1Gw2FkzPhoZyx4Fk9bsXfm301IePp9cwzArI2LdcOhwEZ3RW2F7ie2WJlVy5tzJjMGCaE1tZTjiCahLNEeTiTQp public-key@cloud.tbz.ch
   
    
Die `ssh_...` Einträge erstellen eine Datei `.ssh/authorized_keys`, für den Zugriff via [ssh]((https://wiki.ubuntuusers.de/SSH/), und legen die [Public Keys](https://www.inside-it.ch/de/post/was-ist-eigentlich-20201102) von GitHub User `mc-b` und einen eigendefinierten SSH Key ab.

##### packages

Die Anweisung `sudo: ALL=(ALL) NOPASSWD:ALL` erlaubt es dem User, ohne Password, Superuser im System zu werden und sollte mit Vorsicht verwendet werden.

    packages:
      - unzip
      
Der Eintrag `packages` enthält eine Liste von [Packeten](https://wiki.ubuntuusers.de/APT/) die Installiert werden sollen.

##### runcmd

    runcmd:
      - echo "Hello from Cloud-init"
      
`runcmd` eine Liste von Linux Shell Befehlen, welche abgearbeitet werden soll. 

**Tipp**: bei sehr vielen Befehlen, lohnt es sich diese in einer separaten Scriptdatei abzulegen und z.B. auf GitHub/GitLab abzulegen. So können die Befehle auch getestet werden, ohne jedesmal die VM neu zu erstellen. Die Cloud-init Datei sieht dann wie folgt aus:

    runcmd:
     - curl -sfL https://gitlab.com/ch-tbz-hf/Stud/cnt/-/raw/main/2_Unterrichtsressourcen/B/myscript.sh | bash -

##### write_files

`write_files` dient zum Erzeugen von Konfigurationsdateien. 

Mittels `content:` kann der Inhalt festgelegt werden, dabei ist auf die richtige Einrückung zu achten.
      
    write_files:
     - content: |
        Cloud-init write_files
       path: /etc/issue
       permissions: '0644' 

Das obige Beispiel erzeugt die Willkommensdatei von Linux.

Es können aber auch Webseiten oder Konfigurationsdateien, z.B. für den Apache Web Server erstellt werden:

    write_files:
     - content: |
        <html>
         <body>
          <h1>My Application</h1>
           <ul>
           <li><a href="/order">Order</a></li>
           <li><a href="/customer">Customer</a></li>
           <li><a href="/catalog">Catalog</a></li>
           </ul>
         </body>
        </html>
       path: /var/www/html/index.html
       permissions: '0644'  
     - content: |
        ProxyRequests Off
        <Proxy *>
              Order deny,allow
              Allow from all
        </Proxy>
        ProxyPass /order http://order         
        ProxyPassReverse /order http://order 
        ProxyPass /customer http://customer         
        ProxyPassReverse /customer http://customer  
        ProxyPass /catalog http://catalog         
        ProxyPassReverse /catalog http://catalog 
       path: /etc/apache2/sites-enabled/001-reverseproxy.conf
       permissions: '0644'  

**Tipp**: Sind sehr viele Dateien anzulegen, lohnt es sich, diese zuerst in einem Git Repostory abzulegen und von dort zu kopieren bzw. zu clonen:

    runcmd:
      - ( cd /var/www/html && git clone https://github.com/<user>/<repository> )


### Überprüfung der Installation

Nach der Installation der VM und durchlaufen von Cloud-init kann mittels 

    sudo cloud-init status

überprüft werden, ob Cloud-init ordnungsgemäss durchgelaufen ist.

Bei Fehlern (`error`) sind die Logdateien, zu studieren

* **/var/log/cloud-init.log** – Protokolldatei der ausgeführten Ergebnisses von cloud-init
* **/var/log/cloud-init-output.log** - Ausgabe von cloud-init bzw. deren Befehle

Die verwendete Konfiguration, welche aus `User data` erzeugt wurde, kann im Verzeichnis

* **/var/lib/cloud/instance** - z.B. Datei `user-data.txt` oder `scripts/runcmd` angeschaut werden.

***
### Links

* [Cloud-init Dokumentation](https://cloudinit.readthedocs.io/en/latest/)
* [Ubuntu und Cloud-init](https://help.ubuntu.com/community/CloudInit)
* [AWS und Cloud-init](http://techflare.blog/beginner-tutorial-cloud-init-in-aws/)
* [Azure und Cloud-init](https://docs.microsoft.com/en-us/azure/virtual-machines/custom-data)
* [Google und Cloud-init](https://cloud.google.com/container-optimized-os/docs/how-to/create-configure-instance#using_cloud-init_with_the_cloud_config_format)

***
### Weitere Informationen

**Linux**
* [Linux](https://github.com/mc-b/M300/blob/master/80-Ergaenzungen/linux/01-Linux.md)
* [Bash](https://github.com/mc-b/M300/blob/master/80-Ergaenzungen/linux/10-Bash.md)
* [Advanced Packaging Tool (APT)](https://github.com/mc-b/M300/blob/master/80-Ergaenzungen/linux/20-APTTool.md)
* [Packetverwaltung Tipps](https://wiki.ubuntuusers.de/Paketverwaltung/Tipps/)
* [Public-Private-Key-Verschlüsselung](https://www.inside-it.ch/de/post/was-ist-eigentlich-20201102) und [ssh](https://wiki.ubuntuusers.de/SSH/)
* [cURL](https://github.com/mc-b/M300/blob/master/80-Ergaenzungen/linux/30-cURL.md)

**Software Konfiguration**
* [Einleitung](https://github.com/mc-b/M300/blob/master/80-Ergaenzungen/swkonfiguration/01-Einleitung.md)
* [Tools](https://github.com/mc-b/M300/blob/master/80-Ergaenzungen/swkonfiguration/02-Tools.md)
    
**Service Konfiguration**
* [Einleitung](https://github.com/mc-b/M300/blob/master/80-Ergaenzungen/srvkonfiguration/01-Einleitung.md)
* [systemd](https://github.com/mc-b/M300/blob/master/80-Ergaenzungen/srvkonfiguration/02-systemd.md)
