## Infrastruktur als Code

[[_TOC_]]

![](../x_gitressourcen/cloud-init.png)

- - -

### Einführung 

Infrastruktur als Code ist ein Paradigma (grundsätzliche Denkweise) zur Infrastruktur-Automation.

Es basiert auf konsistenten und wiederholbaren Definitionen (Code) für die Bereitstellung von Systemen und deren Konfiguration.

Produkte sind u.a. Cloud-init, Vagrant, TerraForm etc.

### Definition von Infrastruktur als Code

* Infrastruktur als Code ist ein Ansatz zur Automatisierung der Infrastruktur, der auf Praktiken aus der Softwareentwicklung basiert. 
* Dabei werden konsistente, wiederholbare Prozesse für die Bereitstellung und Änderung von Systemen und deren Konfiguration verwendet.
* Änderungen werden an Deklarationen (Code) vorgenommen und dann durch automatisierte Prozesse, auf Systeme übertragen.
* Infrastruktur als Code hat sich in den anspruchsvollsten Umgebungen bewährt. Für Unternehmen wie Amazon, Netflix, Google und Facebook sind IT-Systeme nicht nur geschäftskritisch. Sie sind das Geschäft!

### Ziele von Infrastruktur als Code

* Die IT-Infrastruktur unterstützt und ermöglicht Veränderungen, anstatt ein Hindernis oder eine Einschränkung zu sein.
* Änderungen am System sind Routine, ohne Drama oder Stress für Benutzer oder IT-Mitarbeiter.
* IT-Mitarbeiter verbringen ihre Zeit mit wertvollen Dingen, die ihre Fähigkeiten einbeziehen, und nicht mit sich wiederholenden Routineaufgaben.
* Benutzer können die benötigten Ressourcen definieren, bereitstellen und verwalten, ohne dass IT-Mitarbeiter dies für sie tun müssen.
* Teams können sich einfach und schnell von Fehlern erholen, anstatt davon auszugehen, dass Fehler vollständig verhindert werden können.
* Verbesserungen werden kontinuierlich vorgenommen und nicht durch teure und riskante „Urknall“ -Projekte.
* Lösungen für Probleme werden durch Implementierung, Test und Messung bewiesen, anstatt sie in Besprechungen und Dokumenten zu diskutieren.

### Implementierung mittels Cloud-init

MAAS Unterstützt dieses Paradigma mittels [Cloud-init](https://cloudinit.readthedocs.io/en/latest/). 

Mittels selektionieren einer oder mehrere VMs und Anwahl des Pulldownmenus `Deploy` erscheint obiger [Dialog](https://maas.io/docs/snap/2.9/ui/custom-machine-setup#heading--cloud-init), wo ein Cloud-init Script angegeben werden kann.

Beispiel für Scripts (mit Ubuntu Linux) sind:

    #cloud-config - Installiert den nginx Web Server
    packages:
     - nginx

- - -

    #cloud-config - Installiert den Apache Web Server mit PHP Unterstuetzung und einer Introseite
    packages:
      - apache2 
      - php 
      - libapache2-mod-php 
    write_files:
     - content: |
         <?php echo '<p>Hallo ich bin eine PHP Datei</p>'; ?>
       path: /var/www/html/index.php
       permissions: '0644'  
          

### Hands-on

**Cloud-init**
* [Cloud-init Hands-on](cloud-init.md)
* [Cloud-init und PHP](cloud-init-php.md)
* [Cloud-init und MySQL](cloud-init-mysql.md)

**YAML**
* [YAML Quick Guide](https://www.tutorialspoint.com/yaml/yaml_quick_guide.htm)

**Git/Versionsverwaltungssysteme und Markdown (Wiki)**
* [Persönliches Portfolio mit GIT und Markdown](https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/02_git/README.md)
* [GitLab](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N0-GitLAB)
* [Markdown Basic Syntax](https://www.markdownguide.org/basic-syntax/)

**Links**

* [Offizielle Cloud-init Beispiele](https://cloudinit.readthedocs.io/en/latest/topics/examples.html)
* [lernMAAS und Cloud-init in der Public Cloud](https://github.com/mc-b/lernmaas/tree/master/doc/Cloud)
* [How To Use Cloud-Config For Your Initial Server Setup](https://www.digitalocean.com/community/tutorials/how-to-use-cloud-config-for-your-initial-server-setup)
* [YAML Tips (nicht nur) für Kubernetes](https://www.youtube.com/watch?v=1rwCkFTjikw&ab_channel=SoulmanIqbal) ab Minute 8: VSCode Extension
* [YAML Link (Validator)](http://www.yamllint.com/)
