###
#   Outputs wie IP-Adresse und DNS Name
#  


output "ip_vm_web" {
  value = module.web.ip_vm
  description = "The IP address of the server instance."
}

output "fqdn_vm_web" {
  value = module.web.fqdn_vm
  description = "The FQDN of the server instance."
}

output "ip_vm_mysql" {
  value = module.mysql.ip_vm
  description = "The IP address of the server instance."
}

output "fqdn_vm_mysql" {
  value = module.mysql.fqdn_vm
  description = "The FQDN of the server instance."
}


# Einfuehrungsseite(n)

output "README" {
  value = templatefile( "INTRO.md", { ip = module.web.ip_vm, fqdn = module.web.fqdn_vm } )
} 
