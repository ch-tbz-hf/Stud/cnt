Cloud-init MySQL Datenbank
--------------------------

Als Datenbank Beispiel eignet sich [MySQL](https://www.mysql.com/de/).

Diese wird nur mit dem `root` User Erstellt. Der Zugriff erfolgt, via der Web Oberfläche von [Adminer](https://www.adminer.org/), siehe vorherige [VM](cloud-init-php.md).

Die MySQL VM braucht kein VPN. Die Verbindung [Adminer](https://www.adminer.org/) zu MySQL erfolgt mittels dem DNS Namen, welcher von MAAS vergeben wird.

* Datenbank System: MySQL
* Server          : `mysql`  
* Benutzer        : `myuser`    
* Passwort        : `mypass`    
* Datenbank       :   

Das Cloud-init Script sieht wie folgt aus:

    #cloud-config
    users:
      - name: ubuntu
        sudo: ALL=(ALL) NOPASSWD:ALL
        groups: users, admin
        home: /home/ubuntu
        shell: /bin/bash
        lock_passwd: false
        plain_text_passwd: 'password'        
    # login ssh and console with password
    ssh_pwauth: true
    disable_root: false    
    packages:
      - mysql-server 
    write_files:
     - content: |
        CREATE USER 'myuser'@'localhost' IDENTIFIED BY 'mypass';
        CREATE USER 'myuser'@'%' IDENTIFIED BY 'mypass';
        GRANT ALL ON *.* TO 'myuser'@'localhost';
        GRANT ALL ON *.* TO 'myuser'@'%';
        FLUSH PRIVILEGES;
       path: /tmp/initdb.sql
       permissions: '0644'  
    runcmd:
      - 'sudo mysql </tmp/initdb.sql'
      - sudo rm /tmp/initdb.sql 
      - sudo sed -i -e"s/^bind-address\s*=\s*127.0.0.1/bind-address = 0.0.0.0/" /etc/mysql/mysql.conf.d/mysqld.cnf
      - sudo systemctl restart mysql
      
