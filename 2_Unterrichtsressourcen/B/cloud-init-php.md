Cloud-init und PHP 
------------------

Um PHP auszuführen braucht es einen Web Server (apache2) mit der PHP Erweiterung.

Zusätzlich installieren wir die MySQL Web Oberfläche [Adminer](https://www.adminer.org/), um nachher auf eine zweite VM mit MySQL zuzugreifen.

Das Cloud-init Script sieht wie folgt aus:

    #cloud-config
    users:
      - name: ubuntu
        sudo: ALL=(ALL) NOPASSWD:ALL
        groups: users, admin
        home: /home/ubuntu
        shell: /bin/bash
        lock_passwd: false
        plain_text_passwd: 'password'        
    # login ssh and console with password
    ssh_pwauth: true
    disable_root: false    
    packages:
      - apache2 
      - curl 
      - wget 
      - php 
      - libapache2-mod-php 
      - php-mysql 
      - adminer
    write_files:
     - content: |
         <?php echo '<p>Hallo ich bin eine PHP Datei</p>'; ?>
       path: /var/www/html/index.php
       permissions: '0644'  
    runcmd:
      - sudo a2enconf adminer
      - sudo systemctl restart apache2
      
Beim Aufruf der IP-Adresse der VM im Browser erscheint der Text der PHP Datei.

Wird hinter der IP-Adresse `/adminer` angefügt, erscheint die Web Oberfläche von [Adminer](https://www.adminer.org/).

- - -

Sollen mehrere PHP Dateien, zur Verfügung gestellt werden, legt man diese zuerst in einem Git Repository ab und holt sie dann mittels `git clone` in die VM.
  
    #cloud-config
    users:
      - name: ubuntu
        sudo: ALL=(ALL) NOPASSWD:ALL
        groups: users, admin
        home: /home/ubuntu
        shell: /bin/bash
        lock_passwd: false
        plain_text_passwd: 'password'        
    # login ssh and console with password
    ssh_pwauth: true
    disable_root: false    
    packages:
      - apache2 
      - curl 
      - wget 
      - php 
      - libapache2-mod-php 
      - php-mysql 
      - adminer
    runcmd:
      - ( cd /var/www/html && git clone https://github.com/<user>/<repository> )
      - sudo a2enconf adminer
      - sudo systemctl restart apache2 
           